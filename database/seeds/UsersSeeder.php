<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->truncate();

        $roleDev = \App\Models\Role::query()->where('slug','dev')->first();
        $roleAdmin = \App\Models\Role::query()->where('slug','admin')->first();

        $user = new User();
        $user->name = 'Dev';
        $user->username = 'dev';
        $user->email = 'dev@udare.com';
        $user->password = 'devdev';
        $user->role_id = $roleDev->_id;
        $user->save();

        $user = new User();
        $user->name = 'Admin Admin';
        $user->username = 'admin';
        $user->email = 'admin@udare.com';
        $user->password = 'adminadmin';
        $user->role_id = $roleAdmin->_id;
        $user->save();
    }
}

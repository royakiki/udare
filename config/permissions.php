<?php


return [
    'dev' => [
        'all'
    ],

    'admin' => [
        'country' => ['add', 'edit', 'delete', 'show'],
        'currency' => ['add', 'edit', 'delete', 'show'],
        'device' => ['add', 'edit', 'delete', 'show'],
        'company' => ['add', 'edit', 'delete', 'show'],
        'general_information' => ['add', 'edit', 'delete', 'show'],
        'license' => ['add', 'edit', 'delete', 'show'],
        'printer' => ['add', 'edit', 'delete', 'show'],
        'role' => ['add', 'edit', 'delete', 'show'],
        'service' => ['add', 'edit', 'delete', 'show'],
        'shift' => ['add', 'edit', 'delete', 'show'],
        'site_timing' => ['add', 'edit', 'delete', 'show'],
        'status' => ['add', 'edit', 'delete', 'show'],
        'transaction' => ['add', 'edit', 'delete', 'show'],
        'price_list' => ['add', 'edit', 'delete', 'show'],
        'site' => ['add', 'edit', 'delete', 'show'],
        'user' => ['add', 'edit', 'delete', 'show'],
    ],

    'venue-owner' => [
        'dashboard' => ['add', 'edit', 'delete', 'show'],
        'venue' => ['add', 'edit', 'delete', 'show'],
        'booking' => ['add', 'edit', 'delete', 'show'],
        'market' => ['add', 'edit', 'delete', 'show'],
        'spread' => ['add', 'edit', 'delete', 'show'],
        'payment' => ['add', 'edit', 'delete', 'show'],
        'user' => ['add', 'edit', 'delete', 'show'],
        'role' => ['add', 'edit', 'delete', 'show'],
        'about' => ['add', 'edit', 'delete', 'show'],
        'contact' => ['add', 'edit', 'delete', 'show'],
    ],

    'user' => [],
];

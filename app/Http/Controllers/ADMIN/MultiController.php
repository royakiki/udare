<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MultiController extends Controller
{
    protected $module;
    protected $validationRules;

    public function __construct($model)
    {
        $this->compacts = [
            'title' => $model->title,
            'fields' => $model->fields,
            'route' => route($model->route.'.index'),
            'new' => route($model->route.'.create')
        ];
        $rules = [];
        foreach ($model->formFields as $name => $field){
            if ( Arr::has($field , 'rules') )
            $rules[$name] =   $field['rules'];
        }
        $this->validationRules = $rules;
        $this->module = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, $this->module->route);
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        if ($request->ajax()) {
                return $this->module->renderDataTable($this->module::query(), $permissions);
        }
        return view('admin.layouts.resources.index', $this->compacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'add');
        $form = $this->module->renderForm(route($this->module->route.'.store'));
        return view('admin.layouts.resources.create', compact('form'), ['title' => $this->compacts['title'],
            'route' => $this->module->route]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'add');
        $this->validate($request, $this->validationRules);
        $allParams = $request->except('_method', '_token');
        $params = [];

        foreach ( $this->module->formFields as $key =>$value ){
            if ( ( $value['insertion_type'] == 'belongsTo' || $value['insertion_type'] == 'field')
                && Arr::has($allParams , $key) ){
                $params[$key] = $allParams[$key];
            }
        }
        $m = $this->module::query()->create($params);
        foreach ( $this->module->formFields as $key =>$value ){
            if( $value['insertion_type'] == 'belongsToMany'){
                if (  Arr::has($allParams , $key))
                $m->{$value['pivot_reference']}()->attach($allParams[$key]);
            }

        }
        $m->save();
        return redirect()->route($this->module->route.'.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'show');
        $form = $this->module->renderForm(route($this->module->route.'.update', $id), $id, 1);
        return view('admin.layouts.resources.show', compact('form'), ['title' => $this->compacts['title'],'route' => $this->module->route]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'edit');
        $form = $this->module->renderForm(route($this->module->route.'.update', $id), $id);
        return view('admin.layouts.resources.edit', compact('form'), ['title' => $this->compacts['title'],'route' => $this->module->route]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'edit');
        //$this->validationRules['name'] .= ',' . $id;
        $this->validate($request, $this->validationRules);
        $allParams = $request->except('_method', '_token');
        $params = [];
        $m = $this->module::query()->findOrFail($id);
        foreach ( $this->module->formFields as $key =>$value ){
            if ( $value['insertion_type'] == 'field'&& Arr::has($allParams , $key)){
                $params[$key] = $allParams[$key];
            }
            if( $value['insertion_type'] == 'belongTo'|| $value['insertion_type'] == 'belongsToMany'){
                $m->{$value['pivot_reference']}()->detach();
                if ( Arr::has($allParams , $key))
                $m->{$value['pivot_reference']}()->attach($allParams[$key]);
            }

        }
        $m->update($params);

        return redirect()->route($this->module->route.'.index');
    }

    public function destroy($id)
    {
        $this->module::destroy($id);
    }

}

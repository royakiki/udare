<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\Field;
use App\Models\Membership;
use App\Models\Sport;
use App\Models\Trainer;
use App\Models\Venue;
use App\Utils\GalleryTrait;
use Illuminate\Http\Request;

class MembershipController extends Controller
{


    use GalleryTrait;


    public function index( Request $request , $venue){
        $membership = new Membership();
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
                    'title' => $venueModel->name."'s Memberships",
                    'fields' => $membership->fields,
                    'route'=> route('admin.membership.index', $venue),
                    'new' => route('admin.membership.create', $venue),
        ];
        $permissions = $this->getPermissions($request->permissions, $membership->route);
        $baseRoute =route('admin.membership.index', $venue);
        if (in_array('new', array_keys($compacts) ) && !in_array('add', $permissions))
            unset( $compacts['new']);
        if ($request->ajax()) {
            return $membership->renderDataTable(Membership::query()->where('venue_id',$venue), $permissions,$baseRoute);
        }
        return view('admin.layouts.resources.index', $compacts);
    }
    public function create($venue)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('admin.membership.index', $venue),
        ];
        $trainers = Trainer::query()->whereHas('venues',function($q) use ($venue){
            $q->where('_id',$venue);
        })->get();

        return view('admin.venue.property.membership.create',compact('venueModel','trainers'),$compacts);
    }
    public function store(Request $request, $venue)
    {
        $this->customValidate($request,[
            'name'=>'required',
            'membership_type_id'=>'required',
            'sport_id'=>'required',
            'status_id'=>'required'
        ]);
        $params = $request->only('sport_id','membership_type_id', 'discount_percentage','discount_days','status_id',
            'description','name','image');
        $params['venue_id']= $venue;
        $membership = Membership::create($params);
        $trainerIds = $request->input('trainer_ids');
        $trainerCosts = $request->input('trainer_monthly_cost');
        $trainerNotes = $request->input('trainer_notes');
        $trainers =[];
        foreach ($trainerIds as $k =>$id ){
            $trainers[]=[
                '_id'=> $id,
                'fees'=>$trainerCosts[$k],
                'notes'=>$trainerNotes[$k]
            ];
        }
        $membership->trainers = $trainers;
        $membership->save();

        return redirect()->route('admin.membership.index',$venue);
    }
    public function edit($venue , $id)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $field = Field::query()->findOrFail($id);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('admin.field.index', $venue),
            'new'=> route('admin.field.index',$venue),
        ];
        return view('admin.venue.property.field.edit',compact('field'),$compacts);
    }
    public function update(Request $request, $venue , $id)
    {
        return redirect()->route('admin.field.index',$venue);
    }


    public function getFieldProperties($sport_id , $field_id = null ){
        $sport = Sport::query()->find($sport_id);
        if ( $field_id != null )
        $field = Field::query()->find($field_id);
        else
            $field = null ;
        return view('admin.venue.property.field.field-inner', compact('sport','field'));
    }



    public function uploadImage( Request $request,$venue, $id){
        $field = Field::query()->findOrFail($id);
        $file = $request->file('file');
        return $this->uploadImageTrait($field, $file,$field->name,'fields');
    }

    public function removeImage( Request $request,$venue, $id){
        $field = Field::query()->findOrFail($id);
        $file = $request->input('file');
        return response()->json(['status'=>$this->removeImageTrait($field, $file)]);
    }

    public function getGallery($venue, $id){
        $field = Field::query()->findOrFail($id);
        return $this->getFilesImageTrait($field);
    }
}

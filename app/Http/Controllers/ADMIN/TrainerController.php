<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\AppUser;
use App\Models\Category;
use App\Models\Field;
use App\Models\Role;
use App\Models\Trainer;
use App\Models\Venue;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TrainerController extends Controller
{
    public function __construct()
    {
        $this->compacts = [
            'title' => 'TRAINERS',
            'fields' => (new AppUser)->fields,
            'route' => route('admin.trainer.index'),
            'new' => route('admin.trainer.create'),
        ];
        $this->validationRules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
//            'phone' => 'required',
            'gender' => 'required|in:male,female,not specified',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required|exists:roles,id',
            'company_id' => 'required|exists:companies,id',
        ];
    }
    public function index(Request $request, $type = null)
    {
        $permissions = $this->getPermissions($request->permissions, 'trainer');
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions))
            unset($this->compacts['new']);
        $this->compacts['route'] = route('admin.trainer.index');
        if (!is_null($type)) {
            unset($this->compacts['new']);
            $permissions = ['show'];
        }
        $user = auth()->user();
        $trainer = new Trainer();
        if ($request->ajax()) {
            $venues = $user->relatedVenues();
            $query = Trainer::query()->whereHas('venues', function($q) use ($venues){
                $q->whereIn('_id',$venues);
            });
            return $trainer->renderDataTable($query, $permissions);
        }

        return view('admin.layouts.resources.index', $this->compacts);
    }

    public function edit($id)
    {
        $trainer = User::findOrFail($id);
        $categories = Category::all();
        return view('admin.trainer.edit', [
            'trainer' => $trainer,
            'categories'=>$categories,
        ]
        );
    }
    public function  create(){
        $categories = Category::all();
        $user = auth()->user();
        $venues = Venue::query()->whereIn('_id',$user->relatedVenues())->get();
        return view('admin.trainer.create',compact('categories','venues'));
    }
    public function store(Request $request){

        $this->customValidate($request, ['name'=>'required',
            'email'=>'required|unique:users',
            'venue_ids'=>'required',
            'sport_ids'=>'required',
            'username'=>'required|unique:users']);
        $params = $request->only('name','email','username');
        $venueIds = $request->input('venue_ids');
        $sportIds = $request->input('sport_ids');
        $venueModel = Venue::query()->find($venueIds[0]);
        if ( $venueModel == null ){
            toastr('venues not found','error');
            return redirect()->back();
        }
        $params['country_id'] = $venueModel->country_id;
        $trainerRole = Role::query()->where('slug','trainer')->first();
        $params['role_id']= $trainerRole->_id;
        $trainer = Trainer::create($params);
        $trainer->venues()->attach($venueIds);
        $trainer->sports()->attach($sportIds);
        return redirect()->route('admin.trainer.index');

    }

}

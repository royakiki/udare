<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\Field;
use App\Models\Sport;
use App\Models\Venue;
use App\Utils\GalleryTrait;
use Illuminate\Http\Request;

class FieldController extends Controller
{


    use GalleryTrait;


    public function index( Request $request , $venue){
        $field = new Field();
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
                    'title' => $venueModel->name."'s Fields",
                    'fields' => $field->fields,
                    'route'=> route('admin.field.index', $venue),
                    'new' => route('admin.field.create', $venue),
        ];
        $permissions = $this->getPermissions($request->permissions, $field->route);
        $baseRoute =route('admin.field.index', $venue);
        if (in_array('new', array_keys($compacts) ) && !in_array('add', $permissions))
            unset( $compacts['new']);
        if ($request->ajax()) {
            return $field->renderDataTable(Field::query()->where('venue_id',$venue), $permissions,$baseRoute);
        }
        return view('admin.layouts.resources.index', $compacts);
    }
    public function create($venue)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('field.index', $venue),
        ];
        return view('admin.venue.property.field.create',compact('venueModel'),$compacts);
    }
    public function store(Request $request, $venue)
    {
        $venueModel = Venue::findOrFail($venue);
        $field = new Field([
            'name' => $request->name,
            'capacity' => $request->capacity,
            'sport_id' => $request->sport_id,
            'zone_id' => $request->zone_id,
            'venue_id' => $venue
        ]);
        $field->save();
        return redirect()->route('field.edit',['venue'=>$venue,'id'=>$field->_id]);
    }
    public function edit($venue , $id)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $field = Field::query()->findOrFail($id);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('admin.field.index', $venue),
            'new'=> route('admin.field.index',$venue),
        ];
        return view('admin.venue.property.field.edit',compact('field'),$compacts);
    }
    public function update(Request $request, $venue , $id)
    {
        return redirect()->route('admin.field.index',$venue);
    }


    public function getFieldProperties($sport_id , $field_id = null ){
        $sport = Sport::query()->find($sport_id);
        if ( $field_id != null )
        $field = Field::query()->find($field_id);
        else
            $field = null ;
        return view('admin.venue.property.field.field-inner', compact('sport','field'));
    }



    public function uploadImage( Request $request,$venue, $id){
        $field = Field::query()->findOrFail($id);
        $file = $request->file('file');
        return $this->uploadImageTrait($field, $file,$field->name,'fields');
    }

    public function removeImage( Request $request,$venue, $id){
        $field = Field::query()->findOrFail($id);
        $file = $request->input('file');
        return response()->json(['status'=>$this->removeImageTrait($field, $file)]);
    }

    public function getGallery($venue, $id){
        $field = Field::query()->findOrFail($id);
        return $this->getFilesImageTrait($field);
    }
}

<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use App\Models\Field;
use App\Models\Link;
use App\Models\Location;
use App\Models\Selection;
use App\Models\Status;
use App\Models\Venue;
use App\Utils\GalleryTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class VenueController extends Controller
{
    //
    use GalleryTrait;

    protected $module;
    protected $validationRules;

    public function __construct()
    {
        $model = new Venue();
        $this->compacts = [
            'title' => $model->title,
            'fields' => $model->fields,
            'route' => route('admin.venue.index'),
            'new' => route('admin.venue.create')
        ];
        $rules = [];
        foreach ($model->formFields as $name => $field){
            if ( Arr::has($field , 'rules') )
                $rules[$name] =   $field['rules'];
        }
        $this->validationRules = $rules;
        $this->module = $model;
    }

    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, $this->module->route);
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        if ($request->ajax()) {
            $baseRoute = route('admin.venue.index');
            return $this->module->renderDataTable(Venue::query()
                ->where('owner_id',auth()->user()->_id)
                ->orWhereIn('_id', auth()->user()->relatedVenues()), $permissions,$baseRoute);
        }
        return view('admin.layouts.resources.index', $this->compacts);
    }


    public function create()
    {

        $categories = Category::all();

        return view('admin.venue.create',[
            'categories' => $categories,
            'title' => $this->compacts['title'],
            'route' => $this->module->route,
        ]);
    }
    public function store(Request $request)
    {

        $this->checkPermission($request->permissions, $this->module->route, 'edit');
        //$this->validationRules['name'] .= ',' . $id;
//        $this->validate($request, $this->validationRules);
//       $this->customValidate($request, ['ahmad'=>'required', 'rabee'=>'required|email']);
        $allParams = $request->except('_method', '_token');
        $params = [];
        $pending = Status::query()->where('slug','pending')->first();
        $m = new Venue();
        $m->email = $request->input('email');
        $m->name = $request->input('name');
        $m->commercial_name = $request->input('commercial_name');
        $m->owner_id = auth()->user()->_id;
        $m->country_id = $request->input('country_id');
        $m->status_id = $pending->_id;
        $m->location_id = $request->input('location_id');

        $m->sports()->sync(is_array($request->input('sport_ids'))?$request->input('sport_ids'):[$request->input('sport_ids')]);
//        $m->locations()->sync(is_array($request->input('location_ids'))?$request->input('location_ids'):[$request->input('location_ids')]);
        $m->selections()->sync(is_array($request->input('selection_ids'))?$request->input('selection_ids'):[$request->input('selection_ids')]);
        $m->save();

        return redirect()->route('admin.venue.edit',$m->_id);
    }

    public function edit($id)
    {
        $venue = Venue::findOrFail($id);
        $categories = Category::all();
        return view('admin.venue.edit', [
            'venue' => $venue,
            'categories'=>$categories,
            ],
        $this->compacts
        );
    }


    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'edit');
        //$this->validationRules['name'] .= ',' . $id;
//        $this->validate($request, $this->validationRules);
        $allParams = $request->except('_method', '_token');
        $params = [];
        $m = Venue::query()->findOrFail($id);
        $m->email = $request->input('email');
        $m->name = $request->input('name');
        $m->commercial_name = $request->input('commercial_name');
        $m->owner_id = $request->input('owner_id');
        $m->country_id = $request->input('country_id');
        $m->status_id = $request->input('status_id');
        $m->location_id = $request->input('location_id');
        $m->sports()->sync(is_array($request->input('sport_ids'))?$request->input('sport_ids'):[$request->input('sport_ids')]);
        $m->selections()->sync(is_array($request->input('selection_ids'))?$request->input('selection_ids'):[$request->input('selection_ids')]);
        $m->save();

        return redirect()->route('admin.venue.index');
    }



    public function uploadImage( Request $request, $id){
        $venue = Venue::query()->findOrFail($id);
        $file = $request->file('file');
        return $this->uploadImageTrait($venue, $file,$venue->name,'venues');
    }

    public function removeImage( Request $request, $id){
        $venue = Venue::query()->findOrFail($id);
        $file = $request->input('file');
        return response()->json(['status'=>$this->removeImageTrait($venue, $file)]);
    }

    public function getGallery( $id){
        $venue = Venue::query()->findOrFail($id);
        return $this->getFilesImageTrait($venue);
    }




    public function editAccountDetails($venue)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        return view('admin.venue.property.account-detail',compact('venueModel'));
    }
    public function updateAccountDetails(Request $request, $venue)
    {
        $this->validate($request, $this->validationRules);
        $venueModel = Venue::findOrFail($venue);
        $account_details['name']= $request->name;
        $account_details['number']=$request->number;
        $account_details['iban_number']=$request->iban_number ;
        $account_details['swift_code']= $request->swift_code ;
        $account_details['trading_license_expiry']=  $request->trading_license_expiry ;

        if( $request->file('confirmation_letter_image') !=null){
            $file = $request->file('confirmation_letter_image');
            $name = 'confirmation_letter_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('confirmationLetters')) {
                Storage::disk('public')->makeDirectory('confirmationLetters');
            }
            if (Storage::disk('public')->putFileAs('confirmationLetters', $file, $name)) {
                $account_details['confirmation_letter_image']  = 'storage/confirmation_letters/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('trading_license') !=null) {
            $file = $request->file('trading_license');
            $name = 'trading_license_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('tradingLicenses')) {
                Storage::disk('public')->makeDirectory('tradingLicenses');
            }
            if (Storage::disk('public')->putFileAs('tradingLicenses', $file, $name)) {
                $account_details['trading_license'] = 'storage/tradingLicenses/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('owner_id') !=null) {
            $file = $request->file('owner_id');
            $name = 'owner_id_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('ownerIds')) {
                Storage::disk('public')->makeDirectory('ownerIds');
            }
            if (Storage::disk('public')->putFileAs('ownerIds', $file, $name)) {
                $account_details['owner_id'] = 'storage/ownerIds/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('owner_passport') !=null) {
            $file = $request->file('owner_passport');
            $name = 'owner_passport_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('ownerPassports')) {
                Storage::disk('public')->makeDirectory('ownerPassports');
            }
            if (Storage::disk('public')->putFileAs('ownerPassports', $file, $name)) {
                $account_details['owner_passport'] = 'storage/ownerPassports/' . $name;
            } else {
                return error();
            }
        }
        $venueModel->account_details = $account_details;
        $venueModel->save();
        return redirect()->route('admin.venue.edit',$venue);
    }


    public function editContentDetails($venue)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $links = Link::all();
        return view('admin.venue.property.content-detail',compact('venueModel','links'));
    }
    public function updateContentDetails(Request $request, $venue)
    {
        dd($request->all());
        $this->validate($request, $this->validationRules);
        $venueModel = Venue::findOrFail($venue);
        $account_details['name']= $request->name;
        $account_details['number']=$request->number;
        $account_details['iban_number']=$request->iban_number ;
        $account_details['swift_code']= $request->swift_code ;
        $account_details['trading_license_expiry']=  $request->trading_license_expiry ;

        if( $request->file('confirmation_letter_image') !=null){
            $file = $request->file('confirmation_letter_image');
            $name = 'confirmation_letter_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('confirmationLetters')) {
                Storage::disk('public')->makeDirectory('confirmationLetters');
            }
            if (Storage::disk('public')->putFileAs('confirmationLetters', $file, $name)) {
                $account_details['confirmation_letter_image']  = 'storage/confirmation_letters/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('trading_license') !=null) {
            $file = $request->file('trading_license');
            $name = 'trading_license_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('tradingLicenses')) {
                Storage::disk('public')->makeDirectory('tradingLicenses');
            }
            if (Storage::disk('public')->putFileAs('tradingLicenses', $file, $name)) {
                $account_details['trading_license'] = 'storage/tradingLicenses/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('owner_id') !=null) {
            $file = $request->file('owner_id');
            $name = 'owner_id_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('ownerIds')) {
                Storage::disk('public')->makeDirectory('ownerIds');
            }
            if (Storage::disk('public')->putFileAs('ownerIds', $file, $name)) {
                $account_details['owner_id'] = 'storage/ownerIds/' . $name;
            } else {
                return error();
            }
        }
        if( $request->file('owner_passport') !=null) {
            $file = $request->file('owner_passport');
            $name = 'owner_passport_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('ownerPassports')) {
                Storage::disk('public')->makeDirectory('ownerPassports');
            }
            if (Storage::disk('public')->putFileAs('ownerPassports', $file, $name)) {
                $account_details['owner_passport'] = 'storage/ownerPassports/' . $name;
            } else {
                return error();
            }
        }
        $venueModel->account_details = $account_details;
        $venueModel->save();
        return redirect()->route('admin.venue.edit',$venue);
    }
}

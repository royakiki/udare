<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->compacts = [
            'title' => 'CURRENCIES',
            'fields' => (new Currency)->fields,
            'route' => route('currency.index'),
            'new' => route('currency.create')
        ];
        $this->validationRules = [
            'name' => 'required|unique:currencies,name',
            'symbol' => 'required',
//            'rate' => 'numeric|max:10000|min:0',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, 'currency');
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        $currency = new Currency();
        if ($request->ajax()) {
            return $currency->renderDataTable(Currency::query(), $permissions);
        }
        return view('cms.layouts.resources.index', $this->compacts);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkPermission(request()->permissions, 'currency', 'add');
        $currency = new Currency();
        $form = $currency->renderForm(route('currency.store'));
        return view('cms.layouts.resources.create', compact('form'), ['title' => $this->compacts['title'],'route'=>'currency']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, 'currency', 'add');
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
        Currency::query()->create($params);
        return redirect()->route('currency.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkPermission(request()->permissions, 'country', 'show');
        $form = (new Currency)->renderForm(route('currency.update', $id), $id, 1);
        return view('cms.layouts.resources.show', compact('form'), ['title' => $this->compacts['title'],'route'=>'currency']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkPermission(request()->permissions, 'country', 'edit');
        $form = (new Currency)->renderForm(route('currency.update', $id), $id);
        return view('cms.layouts.resources.edit', compact('form'), ['title' => $this->compacts['title'],'route'=>'currency']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, 'currency', 'edit');
        $this->validationRules['name'] .= ',' . $id;
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
        Currency::query()->findOrFail($id)->update($params);
        return redirect()->route('currency.index');
    }
}

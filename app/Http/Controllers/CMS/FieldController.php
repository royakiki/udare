<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Field;
use App\Models\Venue;
use Illuminate\Http\Request;

class FieldController extends Controller
{




    public function index( Request $request , $venue){
        $field = new Field();
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
                    'title' => $venueModel->name."'s Fields",
                    'fields' => $field->fields,
                    'route'=> route('field.index', $venue),
                    'new' => route('field.create', $venue),
        ];
        $permissions = $this->getPermissions($request->permissions, $field->route);
        $baseRoute =route('field.index', $venue);
        if (in_array('new', array_keys($compacts) ) && !in_array('add', $permissions))
            unset( $compacts['new']);
        if ($request->ajax()) {
            return $field->renderDataTable(Field::query()->where('venue_id',$venue), $permissions,$baseRoute);
        }
        return view('cms.layouts.resources.index', $compacts);
    }
    public function show($venue , $id)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        dd($venueModel->courts);
    }

    public function create($venue)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('field.index', $venue),
        ];
        return view('cms.venue.property.field.create',compact('venueModel'),$compacts);
    }
    public function store(Request $request, $venue)
    {
        $venueModel = Venue::findOrFail($venue);
        $field = new Field([
            'name' => $request->name,
            'capacity' => $request->capacity,
            'sport_id' => $request->sport_id,
            'zone_id' => $request->zone_id,
            'venue_id' => $venue
        ]);
        $field->save();
        return redirect()->route('field.index',$venue);
    }
    public function edit($venue , $id)
    {
        $venueModel = Venue::query()->findOrFail($venue);
        $compacts = [
            'title' => $venueModel->name."'s Fields",
            'route'=> route('field.index', $venue),
        ];
        return view('cms.venue.property.field.edit',compact('venueModel'),$compacts);
    }
    public function update(Request $request, $venue , $id)
    {
        return redirect()->route('field.index',$venue);
    }

}

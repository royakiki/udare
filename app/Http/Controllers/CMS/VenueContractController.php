<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Mail\SendContractEmail;
use App\Models\Contract;
use App\Models\Sport;
use App\Models\Status;
use App\Models\Venue;
use App\Models\VenueContract;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\Engines\Engine;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class VenueContractController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new VenueContract());
    }


    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, $this->module->route);
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        $baseRoute = route('venue-contract.index');
        if ($request->ajax()) {
            return $this->module->renderDataTable($this->module::query(), $permissions,$baseRoute);
        }
        return view('cms.layouts.resources.index', $this->compacts);
    }


    public function create()
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'add');

        return view('cms.venue-contract.create', ['title' => $this->compacts['title'],
            'route' => $this->module->route]);
    }

    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'add');
        $params = $this->validate($request, [
            'contract_id'=>'required',
            'venue_id'=>'required',
        ]);

        $m = VenueContract::query()->create($params);
        $m->status_id = Status::query()->where('slug','pending')->first()->_id;
        $sport_ids = $request->input('sport_ids');
        $commissions = $request->input('sport_commissions');
        $notes = $request->input('commission_notes');
        $sports = [];
        foreach ( $sport_ids as $k => $value){
            $sport= Sport::query()->find($value);
            $sports[] = ['sport_id'=>$value ,'name'=>$sport->name, 'commission'=>$commissions[$k] , 'notes'=>$notes[$k]];
        }
        $m->sports = $sports;
        $m->save();

        return redirect()->route($this->module->route.'.index');
    }


    public function edit($id)
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'edit');
        $venueContract = VenueContract::query()->find($id);

        return view('cms.venue-contract.edit', compact('venueContract'),
            ['title' => $this->compacts['title'],'route' => $this->module->route]);
    }

    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'edit');

        $params = $this->validate($request, [
            'contract_id'=>'required',
            'venue_id'=>'required',
        ]);

        $venueContract = VenueContract::query()->find($id);

        $venueContract->status_id = Status::query()->where('slug','pending')->first()->_id;
        $sport_ids = $request->input('sport_ids');
        $commissions = $request->input('sport_commissions');
        $notes = $request->input('commission_notes');
        $sports = [];
        foreach ( $sport_ids as $k => $value){
            $sport= Sport::query()->find($value);
            $sports[] = ['sport_id'=>$value ,'name'=>$sport->name, 'commission'=>$commissions[$k] , 'notes'=>$notes[$k]];
        }
        $venueContract->sports = $sports;
        $venueContract->save();

        if ( $request->has('email') ){
            $status = Status::query()->where('slug','in-review')->first();
            $venueContract->status_id = $status->_id;
            $pdf =  PDF::loadView('cms.venue-contract.preview', compact('venueContract'));
            if ( !Storage::disk('public')->exists('mailContracts'))
                Storage::disk('public')->makeDirectory("mailContracts");
            $title = 'storage/mailContracts/'.$venueContract->venue->name.time().'-mailed.pdf';
            $pdf->setWarnings(false)->save($title);
            $venueContract->mailUrl = url($title);
            $venueContract->uploaded_contract = null;
            try{
                Mail::to($venueContract->venue->owner->email)->send(new SendContractEmail($venueContract));
            }catch (\Exception $e){

            }

            $venueContract->save();
        }



        if ( $request->has('contract')) {
//            $venueContract->addMedia(storage_path('tmp/uploads/' . $request->input('contract')))
//                ->toMediaCollection('contracts');
            $title = 'contracts/'.$venueContract->venue->name.time().'-uploaded.pdf';
            if ( !Storage::disk('public')->exists('contracts'))
                Storage::disk('public')->makeDirectory("contracts");
            Storage::disk('public')->move('tmp/uploads/'.$request->input('contract'),$title);
            $venueContract->uploaded_contract = 'storage/'.$title;
            $venueContract->mailUrl = null;
            $status = Status::query()->where('slug','active')->first();
            $venueContract->status_id = $status->_id;
            $venueContract->save();
        }

        if ( !$request->has('contract') && $venueContract->uploaded_contract != null){
            $venueContract->uploaded_contract = null;
            $venueContract->mailUrl = null;
            $status = Status::query()->where('slug','pending')->first();
            $venueContract->status_id = $status->_id;
            $venueContract->save();
        }


        return redirect()->route($this->module->route.'.index');
    }


    public function preview($id){
        $venueContract = VenueContract::query()->findOrFail($id);
        if ( $venueContract->mailUrl != null )
            return redirect()->to($venueContract->mailUrl);

        if ( $venueContract->uploaded_contract != null )
            return redirect()->to($venueContract->uploaded_contract);

        return  PDF::loadView('cms.venue-contract.preview', compact('venueContract'))->stream();
    }

    public function download($id){
        $venueContract = VenueContract::query()->findOrFail($id);
        if ( $venueContract->mailUrl != null )
            return redirect()->to($venueContract->mailUrl);

        if ( $venueContract->uploaded_contract != null )
            return redirect()->to($venueContract->uploaded_contract);

        $pdf =  PDF::loadView('cms.venue-contract.preview', compact('venueContract'));
        return $pdf->download($venueContract->venue->name.'.pdf');
    }

}

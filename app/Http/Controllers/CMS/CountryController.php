<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->compacts = [
            'title' => 'COUNTRIES',
            'fields' => (new country)->fields,
            'route' => route('country.index'),
            'new' => route('country.create')
        ];
        $this->validationRules = [
            'name' => 'required|unique:countries,name',
            'code' => 'required'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, 'country');
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        $country = new Country();
        if ($request->ajax()) {
            return $country->renderDataTable(Country::query()->select(array_column($country->fields,'db_name')), $permissions);
        }
        return view('cms.layouts.resources.index', $this->compacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkPermission(request()->permissions, 'country', 'add');
        $country = new Country();
        $form = $country->renderForm(route('country.store'));
        return view('cms.layouts.resources.create', compact('form'), ['title' => $this->compacts['title']
        ,'route'=>'country']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, 'country', 'add');
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
        Country::query()->create($params);
        return redirect()->route('country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkPermission(request()->permissions, 'country', 'show');
        $form = (new Country)->renderForm(route('country.update', $id), $id, 1);
        return view('cms.country.show', compact('form'), ['title' => $this->compacts['title'],'route'=>'country']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkPermission(request()->permissions, 'country', 'edit');
        $form = (new Country)->renderForm(route('country.update', $id), $id);
        return view('cms.layouts.resources.edit', compact('form'), ['title' => $this->compacts['title'],'route'=>'country']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, 'country', 'edit');
//        $this->validationRules['name'] .= ',' . $id;
//        $this->validate($request, $this->validationRules);
        $country=Country::findOrFail($id);
        $this->validate($request, [
            'name' => [
                'required',
                    Rule::unique('countries')->ignore($country),
                ],
            'code' => 'required'
        ]);
        $params = $request->except('_method', '_token');
        $country->update($params);
        return redirect()->route('country.index');
    }
    public function destroy($id){
        Country::query()->findOrFail($id)->delete();
    }

    public function getPhoneCode(Request $request)
    {
        $country = Country::findOrFail($request->countryId);
//        if($messages == []){
//            return response()->json(['status'=>'failed','messages' => $messages ]);
//        }
        return response()->json(['status'=>'success','phone_code' => $country->phone_code ]);
    }

}

<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Models\Feature;
use Illuminate\Http\Request;

class FeatureController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Feature());
    }


//    public function create(){
//        return view('cms.front.feature.create');
//    }
//    public function store(Request $request){
//        return ;
//    }
//    public function edit($id)
//    {
//        $feature = Feature::findOrFail($id);
//        return view('cms.front.feature.edit', compact('feature'));
//    }
//    public function update(Request $request,$id){
//        $feature = Feature::findOrFail($id);
//        return redirect()->route('feature.index');
//    }
}


<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Http\Controllers\Controller;
use App\Models\Registration;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    //
//    public function __construct()
//    {
//        parent::__construct(new Registration());
//    }
    public function index()
    {
        $registration = Registration::query()->first();
        if ($registration == null)
            return $this->create();
        return $this->edit($registration->id);
    }


    public function create(){
        return view('cms.front.registration.create');
    }
    public function store(Request $request){
        $registration = new Registration([
            'text' => $request->text,
        ]);
        $registration->save();
        return redirect()->route('registration.index');
    }
    public function edit($id)
    {
        $registration = Registration::findOrFail($id);
        return view('cms.front.registration.edit', compact('registration'));
    }
    public function update(Request $request,$id){
        $registration = Registration::findOrFail($id);
        $registration->text  =  $request->text;
        $registration->save();
        return redirect()->route('registration.index');
    }
}


<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Models\Value;
use App\Models\Solution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ValuesController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Value());
    }
    public function store(Request $request)
    {
        $value = new Value();
        $value->name = $request->name;
        $value->body = $request->body;
        $file = $request->file('image');
        $name = 'value_' . time() . '.' . $file->getClientOriginalExtension();
        if (!Storage::disk('public')->exists('values')) {
            Storage::disk('public')->makeDirectory('values');
        }
        if (Storage::disk('public')->putFileAs('values', $file, $name)) {
            $value->image = 'values/' . $name;
        } else {
            return error();
        }
        $value->save();
        return redirect()->route('value.index');
    }
}


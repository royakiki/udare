<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->compacts = [
            'title' => 'ROLES',
            'fields' => (new Role)->fields,
            'route' => route('role.index'),
            'new' => route('role.create')
        ];
        $this->validationRules = [
            'name' => 'required|unique:countries,name',
            'status_id' => 'required|exists:statuses,id',
            'service_id' => 'required|exists:services,id',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = $this->getPermissions($request->permissions, 'role');
        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions)) unset($this->compacts['new']);
        $country = new Role();
        if ($request->ajax()) {
            return $country->renderDataTable(Role::query() , $permissions);
        }
        return view('cms.layouts.resources.index', $this->compacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkPermission(request()->permissions, 'role', 'add');
        $country = new Role();
        $form = $country->renderForm(route('role.store'));
        return view('cms.layouts.resources.create', compact('form'), ['title' => $this->compacts['title'],'route'=>'role']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, 'role', 'add');
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
//        DB::transaction(function () use ($params, $request) {
//            $role_id = Role::query()->create($params)->id;
//            Role::query()->findOrFail($role_id)->services()->sync($request->service_id);
//            Role::query()->findOrFail($role_id)->statuses()->sync($request->status_id);
//        });
//        Role::query()->create($params);
        return redirect()->route('role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkPermission(request()->permissions, 'role', 'show');
        $form = (new Role)->renderForm(route('role.update', $id), $id, 1);
        return view('cms.layouts.resources.show', compact('form'), ['title' => $this->compacts['title'],'route'=>'role']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkPermission(request()->permissions, 'role', 'edit');
        $form = (new Role)->renderForm(route('role.update', $id), $id);
        return view('cms.layouts.resources.edit', compact('form'), ['title' => $this->compacts['title'],'route'=>'role']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->checkPermission(request()->permissions, 'role', 'edit');
        $this->validationRules['name'] .= ',' . $id;
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
//        DB::transaction(function () use ($params, $request, $id) {
//            $role_id = Role::query()->findOrFail($id)->update($params);
//            Role::query()->findOrFail($id)->services()->sync($request->service_id);
//            Role::query()->findOrFail($id)->statuses()->sync($request->status_id);
//        });
//        Role::query()->findOrFail($id)->update($params);
        return redirect()->route('role.index');
    }
}

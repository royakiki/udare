<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Mail\NewVenueVerification;
use App\Models\Category;
use App\Models\Country;
use App\Models\Role;
use App\Models\Sport;
use App\Models\Status;
use App\Models\Venue;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class HomeController extends Controller
{
    //

    public function registerForm ( Request $request ){
        $countries = Country::all();
        return view('front.venue.register', compact('countries'));
    }

    public function register( Request $request ){

        $this->customValidate($request, [
            'name'=>'required',
            'email'=>'required|unique:users',
            'phone'=>'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'country_id'=>'required'
        ],[],[],'toast-bottom-left');

        $params = $this->validate($request, [
            'name'=>'required',
            'email'=>'required|unique:users',
            'phone'=>'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'country_id'=>'required'
        ]);

        $this->customValidate($request, [
            'venue_name'=>'required',
            'country_id'=>'required',
            'location_id'=>'required',
        ],[],[],'toast-bottom-left');

        $venueParams = $this->validate($request, [
            'venue_name'=>'required',
            'country_id'=>'required',
            'location_id'=>'required',
        ]);
        $user = null;
        $venue = null;

        try{
            $params['role_id'] = Role::query()->where('slug', 'venue-owner')->first()->id;
            $params['status_id'] = Status::query()->where('slug','pending')->first()->_id;
//
            $user = User::create($params);
//
            $vparams = [];
            $vparams['name'] = $venueParams['venue_name'];
            $vparams['commercial_name'] = $venueParams['venue_name'];
            $vparams['country_id'] = $venueParams['country_id'];
            $vparams['owner_id'] = $user->_id;
            $vparams['location_id'] =  $venueParams['location_id'];
            $vparams['status_id'] = Status::query()->where('slug','pending')->first()->_id;

            $venue = Venue::create($vparams);

            $venue->selections()->attach($request->input('selection_ids'));
            $user->save();

            $user->email_verification_token = Str::random(32);
            $user->save();


            try{
                Mail::to($user->email)->send(new EmailVerification($user));
            }catch (\Exception $e){

            }
            $admins = User::query()->whereHas('role', function ($q){
                $q->whereIn('slug',['admin','dev']);
            })->get();
            foreach ( $admins as $admin ){
                try{
                    Mail::to($admin->email)->send(new NewVenueVerification($admin, $venue));
                }catch (\Exception $e){

                }

            }
            toastr('User and venue has been created successfully, please check your email to validate your personal account. uDare team will contact you for venue account validation.'
            ,'success','',['positionClass'=>'toast-bottom-left',"closeButton"=> true]);
            return redirect()->route('request-venue-categories',$venue->_id);
        }catch (\Exception $e){
            if ( $user != null ){
                $user->forceDelete();
            }
            if ( $venue != null){
                $venue->forceDelete();
            }
            toastr('something went wrong'
                ,'error','',['positionClass'=>'toast-bottom-left',"closeButton"=> true]);
            return redirect()->back();
        }
    }
    public function categoriesForm($venue_id){
        $categories = Category::query()->get();

        return view('front.venue.categories',compact('categories','venue_id'));
    }
    public function categories(Request $request){
//        dd($request->all());
        $venue = Venue::findOrFail($request->venue_id);
        $venue->sports()->attach( $request->input('sport_ids') );
        $venue->other_sport= $request->input('other_sport_names');
        $venue->save();
        toastr("you has been submitted, uDare team will contact you shortly for validation"
            ,'success','',['positionClass'=>'toast-top-right',"closeButton"=> true]);
        Auth::logout();
        return redirect()->route('home');

//        return view('front.venue.categories',compact('categories'));
    }

}

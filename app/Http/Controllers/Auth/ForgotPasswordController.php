<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Mail\PasswordResetLink;
use App\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {

        $user = User::query()->where('email', '=', $request->email)->first();
        if ($user == null) {
            toastr(trans('User does not exist'),'error','',["positionClass"=> "toast-bottom-left","closeButton"=> true]);
            return redirect()->back();
        }
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);
        $tokenData = DB::table('password_resets')->where('email', $request->email)->first();

        if ($this->sendResetEmail($request->email, $tokenData['token'])) {
            toastr('A reset link has been sent to your email address.','success','',["positionClass"=> "toast-bottom-left","closeButton"=> true]);
            return redirect()->back();
        } else {
            toastr('A Network Error occurred. Please try again.','error','',["positionClass"=> "toast-bottom-left","closeButton"=> true]);
            return redirect()->back();
        }
    }

    private function sendResetEmail($email, $token)
    {
        $url = url(route('password.reset', [
            'token' => $token,
            'email' => $email,
        ], false));
       try{
           Mail::to($email)->send(new PasswordResetLink($url));
           return true;
       }catch (\Exception $e){

       }
        return false;
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function reset(Request $request)
    {
        //Validate input
        $this->customValidate($request , [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed',
            'token' => 'required' ]);



    $password = $request->password;

    $tokenData = DB::table('password_resets')->where('token', $request->token)->first();

    if (!$tokenData) return view('auth.passwords.email');

    $user = User::query()->where('email', $tokenData['email'])->first();

    if (!$user){
        toastr('Email not found.','error');
        return redirect()->back();
    }


    $user->password = $password;
    $user->update();

    Auth::loginUsingId($user->id);
//    Auth::login($user);

    DB::table('password_resets')->where('email', $user->email)->delete();
    return redirect()->route('admin.dashboard');
    }

}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->redirectTo = abort(404);
        $this->middleware('guest')->except('logout');

    }

    public function username()
    {
        return 'email';
    }


    protected function authenticated(Request $request, $user)
    {

        $cmsRoles = ['admin','dev','admin-moderator'];
        $adminRoles = ['venue-owner','dev','venue-moderator','trainer'];
        $u = User::query()->findOrFail($user->_id);
        if (in_array($u->role->_id, $cmsRoles ) ) {
            $this->redirectTo = '/cms';
        }else if ( in_array($user->role->_id, $adminRoles ) ){
            $this->redirectTo='/admin';
        }else{
            $this->redirectTo="/";
        }
        if ( !Auth::user()->allowToAccessAdmin() ){
            Auth::logout();
            toastr('you account is not valid or your venue still not approved','error', '',['classPosition'=>'toast-bottom-left']);
            return redirect()->route('login');
        }
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        toastr('The combination of email and password are wrong','error','',["positionClass"=> "toast-bottom-left","closeButton"=> true]);
        return redirect()->back();
    }
}

<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class VenueContract extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "venue-contract";

    public $title = "Venue Contracts";


    protected $fillable = [
        'status_id','contract_id','venue_id'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'venue',
            'show' => 'name',
            'title' => 'Venue',
            'type' => 'object',
            'chains' => 'venue',
            'db_name' => 'name'
        ],
        [
            'key' => 'contract',
            'show' => 'name',
            'title' => 'Contract Name',
            'type' => 'object',
            'chains' => 'contract',
            'db_name' => 'name'
        ],
        [
            'key' => 'status',
            'show' => 'name',
            'title' => 'Status',
            'type' => 'object',
            'chains' => 'status',
            'db_name' => 'name'
        ],

    ];

    public $formFields = [

    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function venue()
    {
        return $this->belongsTo(Venue::class, 'venue_id');
    }
    public function contract(){
        return $this->belongsTo(Contract::class);
    }

    public function sports(){
        return $this->hasMany(Sport::class,'sport_ids');
    }
}

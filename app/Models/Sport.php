<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Sport extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "sport";

    public $title = "SPORT";


    protected $fillable = [
        'name', 'image','color','parent_id','percentage','status_id',
        'attendance_type','appearance','trainer_need','zone_id'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'selection' => 'name',
            'db_name' => 'name'
        ],
        [
            'key' => 'category',
            'show' => 'name',
            'title' => 'Parent',
            'type' => 'object',
            'chains' => 'category',
            'db_name' => 'name'
        ],

    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'percentage' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Percentage',
            'id' => 'percentage',
            'name' => 'percentage',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'appearance' => [
            'references' => [''=>'Select Appearance','post' =>'Post', 'kick-starter'=>'Kick Starter'],
            'input' => 'select',
            'label' => 'uMatch Appearance',
            'id' => 'appearance',
            'name' => 'appearance',
            'isRequired' => 'true',
            'displayMember' => 'name',
            'valueMember' => 'id',
            'insertion_type'=>'field'
        ],
        'attendance_type' => [
            'references' => [''=>'Select Attendance Type','single' =>'Single', 'multi'=>'Multi','memberships'=>'Day Pass or membership'],
            'input' => 'select',
            'label' => 'Attendance Type',
            'id' => 'attendance_type',
            'name' => 'attendance_type',
            'isRequired' => 'true',
            'displayMember' => 'name',
            'valueMember' => 'id',
            'insertion_type'=>'field'
        ],
        'trainer_need' => [
            'input' => 'checkbox',
            'type' => 'checkbox',
            'label' => 'Trainer Need?',
            'id' => 'trainer_need',
            'name' => 'trainer_need',
            'isRequired' => true,
            'classes' => '',
            'insertion_type'=>'field'
        ],
        'image' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'logo',
            'name' => 'image',
            'isRequired' => false,
            'custom' => true,
            'insertion_type'=>'field',
        ],
        'color' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'color',
            'id' => 'color',
            'name' => 'color',
            'isRequired' => false,
            'classes' => 'colorpicker',
            'insertion_type'=>'field'
        ],
        'parent_id' => [
            'references' => 'App\\Models\\Category',
            'input' => 'select',
            'label' => 'Category',
            'id' => 'parent_id',
            'name' => 'parent_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'category',
            'insertion_type'=>'belongsTo'
        ],

        'zone_id' => [
            'references' => 'App\\Models\\Zone',
            'input' => 'select',
            'label' => 'Zone',
            'id' => 'zone_id',
            'name' => 'zone_id',
            'isRequired' => true,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'zone',
            'insertion_type'=>'field'
        ],
        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type'=>'field'
        ],

        'countries_ids' => [
            'references' => 'App\\Models\\Country',
            'input' => 'select',
            'label' => 'Country',
            'id' => 'countries_ids',
            'name' => 'countries_ids',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => true,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'countries',
            'rules'=>'required',
            'insertion_type'=>'belongsToMany'
        ],
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id');
    }


    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

}

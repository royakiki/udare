<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes, WidgetRender;

    protected $fillable = ['name', 'color'];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'statuses.name'
        ],
    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => 'true',
            'classes' => ''
        ],
        'color' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'color',
            'id' => 'color',
            'name' => 'color',
            'isRequired' => 'true',
            'classes' => 'colorpicker'
        ],
    ];

    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('order', 'report_id', 'pay');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'transaction_status');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'transaction_status');
    }

    public function handler()
    {
        return $this->belongsToMany(User::class, 'transaction_status', 'handled_by');
    }
}

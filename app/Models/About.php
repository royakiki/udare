<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class About extends Model
{
    //
    use SoftDeletes, WidgetRender;

    public $route = "about";
    public $title = "About";

    protected $fillable =['about','email','address','phone','logo'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];
    public $formFields = [
        'about' => [
            'input' => 'textarea',
            'type' => 'text',
            'label' => 'About',
            'id' => 'about',
            'name' => 'about',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'address' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Address',
            'id' => 'address',
            'name' => 'address',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'email' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Email',
            'id' => 'email',
            'name' => 'email',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'phone' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Phone',
            'id' => 'phone',
            'name' => 'phone',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'logo' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Logo Link',
            'id' => 'logo',
            'name' => 'logo',
            'isRequired' => true,
            'custom' => true,
            'insertion_type'=>'field',
        ],
    ];
}

<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Field extends Model
{
    //
    use SoftDeletes, WidgetRender;

    public $route = "field";
    public $title = "Fields";

    protected $fillable =['name','zone_id','sport_id', 'capacity','venue_id'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'name'
        ],
        [
            'key' => 'capacity',
            'title' => 'Capacity',
            'type' => 'field',
            'db_name' => 'capacity'
        ],
    ];

    public function venue(){
        return $this->belongsTo(Venue::class,'venue_id');
    }
    public function zone(){
        return $this->belongsTo(Zone::class);
    }
    public function sport(){
        return $this->belongsTo(Sport::class);
    }

    public function slots(){
        return $this->hasMany(TimeSlot::class, 'field_id');
    }
}

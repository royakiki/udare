<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes, WidgetRender;

    protected $fillable = [
        'name', 'symbol', 'rate'
    ];

    protected $casts = ['rate' => 'float'];

    public $fields = [
        [
            'key' => 'name', 'title' => 'name',
            'type' => 'field', 'db_name' => 'currencies.name'
        ],
        [
            'key' => 'symbol', 'title' => 'symbol',
            'type' => 'field', 'db_name' => 'currencies.symbol'
        ],
//        [
//            'key' => 'rate', 'title' => 'rate',
//            'type' => 'field', 'db_name' => 'currencies.rate'
//        ],
    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => 'true',
            'classes' => ''
        ],
        'symbol' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Symbol',
            'id' => 'symbol',
            'name' => 'symbol',
            'isRequired' => 'true',
            'classes' => '',
        ],
        'image_link' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'logo',
            'name' => 'image',
            'isRequired' => false,
            'custom' => true
        ],
//        'rate' => [
//            'input' => 'textbox',
//            'type' => 'number',
//            'label' => 'Rate',
//            'id' => 'rate',
//            'name' => 'rate',
//            'isRequired' => 'true',
//            'classes' => ''
//        ],
    ];

    public function priceLists()
    {
        return $this->hasMany(PriceList::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
    public function serviceHourlyRate()
    {
        return $this->hasMany(ServiceHourlyRate::class);
    }
}

<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Builder;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class AppUser extends User
{
    //
    use SoftDeletes, WidgetRender;

    protected $table = "users";

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'users.name'
        ],
    ];

    public static function boot()
    {
        parent::boot();
        $role = Role::query()->where('slug', 'user')->first()->_id;
        static::addGlobalScope('userRole', function (Builder $builder) use ($role)  {
            $builder->where('role_id', $role );

        });
    }


    public function role(){
        return $this->belongsTo(Role::class);
    }
}

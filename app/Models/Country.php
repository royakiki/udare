<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes, WidgetRender;

    protected $fillable = [
        'name', 'code','phone_code'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'db_name' => 'name'
        ],
        [
            'key' => 'code',
            'title' => 'code',
            'type' => 'field',
            'db_name' => 'code'
        ],

    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => 'true',
            'classes' => ''
        ],
        'code' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Code',
            'id' => 'code',
            'name' => 'code',
            'isRequired' => 'true',
            'classes' => ''
        ],
        'phone_code' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Phone Code',
            'id' => 'phone_code',
            'name' => 'phone_code',
            'isRequired' => 'true',
            'classes' => ''
        ],
    ];

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public function sites()
    {
        return $this->hasMany(Site::class);
    }
    public function currency()
    {
        return $this->hasOne(Currency::class);
    }
}

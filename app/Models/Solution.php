<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Solution extends Model
{
    //
    use SoftDeletes, WidgetRender;

    public $route = "solution";
    public $title = "Solutions";

    protected $fillable =['name','body','image','order'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Title',
            'type' => 'field',
            'db_name' => 'title'
        ],
    ];
    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Title',
            'id' => 'title',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'body' => [
            'input' => 'textarea',
            'type' => 'text',
            'label' => 'Body',
            'id' => 'body',
            'name' => 'body',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'image' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'image',
            'name' => 'image',
            'isRequired' => true,
            'custom' => true,
            'insertion_type'=>'field',
        ],
    ];
}

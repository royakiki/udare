@component('mail::message')
    # Introduction

    Find attach your contract

    @component('mail::button', ['url' => route('api.verify',$venueContract->mailUrl)])
        Button Text
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent

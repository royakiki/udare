@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>New Trainer</h2>
                    <ul class="nav navbar-right panel_toolbox">
{{--                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))--}}
{{--                            <li>--}}
{{--                                <a href="@if(!isset($action)){{ route(str_replace(' ','_',strtolower(Str::singular($route)).'.index')) }}@else {{ $action }}@endif">--}}
{{--                                    <i class="fa fa-list"></i>--}}
{{--                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endif--}}
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('admin.trainer.store')}}" class="form-horizontal form-label-left"
                              enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div  type="button" class="clp" >
                                <h3>Trainer</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>

                            <div id="overview" class="row clpc">
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="name" name="name" required
                                                   value="{{old('name')}}" class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Username
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="username" name="username" required
                                                   value="{{old('username')}}" class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Email
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="email" id="email" name="email" required
                                                   value="{{old('email')}}" class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Description
                                        </label>
                                        <div class="col-md-8 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                //TODO add image
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="status_id">
                                            Venue
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-without-ajax"
                                                    name="venue_ids[]"
                                                    multiple
                                                    required>
                                                <option value=""> Select venue</option>
                                                @foreach($venues as $venue)
                                                    <option value="{{$venue->_id}}"
                                                    @if(old('venue_ids') != null and in_array($venue->_id, old('venue_ids')) ) selected @endif>
                                                        {{$venue->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-8" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="status_id">
                                            Status
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="status_id"
                                                    id="status_id"
                                                    data-route="{{route('select.ajax','Status')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                @if(old('status_id') != null)
                                                    <option value="{{old('status_id')}}" selected>
                                                        {{\App\Models\Status::find(old('status_id'))->name}}
                                                    </option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <div type="button" class="clp">
                                <h3>Sports & Categories</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>
                            <div id="sports" class="row clpc">
                                @foreach( $categories as $category)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="{{$category->_id}}">
                                                {{$category->name}}
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2 select2-ajax"
                                                        name="sport_ids[]"
                                                        id="{{$category->_id}}"
                                                        data-route="{{route('select.ajax','Sport')}}?parent_id={{$category->_id}}"
                                                        data-name="name"
                                                        data-value="_id"
                                                        multiple="multiple">
                                                    @if(old('sport_ids') != null)
                                                        @foreach(old('sport_ids') as $sport_id)
                                                            @if($category->_id == \App\Models\Sport::find($sport_id)->category->_id)
                                                                <option value="{{$sport_id}}" selected>
                                                                    {{\App\Models\Sport::find($sport_id)->name}}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- /.col-* -->
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                @endforeach


                            </div><!-- /.row -->
                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Create
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    @include('admin.layouts.form.javascript')
    <script>
        //Language
        // ajaxSelect('language_ids','Languages')
        //Selection
        ajaxSelect('selection_ids','Selections')
        //Category
        @foreach( $categories as $category)
        ajaxSelect('{{$category->_id}}','Category')
        @endforeach
        //Category (Other)
        $(".select2-ajax-add").select2({
            tags: true,
            width: '100%',
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
        ajaxSelect('status_id','Status')
    </script>
@endpush

@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> New Membership </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))
                            <li>
                                <a href="{{$route}}">
                                    <i class="fa fa-list"></i>
                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('admin.membership.store',$venueModel->_id)}}"
                              class="form-horizontal form-label-left">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12"
                                               for="name">Name *</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="name" name="name" required value="{{old('name')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="membership_type_id">
                                            Type *
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="membership_type_id"
                                                    id="membership_type_id"
                                                    data-route="{{route('select.ajax','MembershipType')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                @if(old('membership_type_id') != null)
                                                    <option value="{{old('membership_type_id')}}" selected>
                                                        {{\App\Models\MembershipType::find(old('membership_type_id'))->name}}
                                                    </option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="sport_id">
                                            Sport *
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="sport_id"
                                                    id="sport_id"
                                                    data-route="{{route('select.ajax','Sport')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                @if(old('sport_id') != null)
                                                    <option value="{{old('sport_id')}}" selected>
                                                        {{\App\Models\Sport::find(old('sport_id'))->name}}
                                                    </option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="status_id">
                                            Status *
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="status_id"
                                                    id="status_id"
                                                    data-route="{{route('select.ajax','Status')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                @if(old('status_id') != null)
                                                    <option value="{{old('sport_id')}}" selected>
                                                        {{\App\Models\Status::find(old('status_id'))->name}}
                                                    </option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="discount_percentage">
                                            Discount Percentage</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="discount_percentage" name="discount_percentage"
                                                   value="{{old('discount_percentage')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="discount_days">
                                            Discount Days</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="discount_days" name="discount_days"
                                                   value="{{old('discount_days')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="description">
                                            Description</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="description" name="description"
                                                   value="{{old('description')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->


                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="image">
                                            Image</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="file" id="image" name="image"
                                                   value="{{old('image')}}"
                                                   class=" col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div>
                            <div class="ln_solid"></div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sport_ids">Trainers </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="trainers">
                                            <div id="trainers-box"></div>
                                            <br/>
                                            <br/>
                                            <div class="btn btn-primary" onclick="addTrainer()">Add Trainer</div>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- row -->
                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Create
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    @include('cms.layouts.form.javascript')
    <script>
        ajaxSelect('sport_id','Sport')
        ajaxSelect('membership_type_id','MembershipType')
        ajaxSelect('status_id','Status')


        function addTrainer(){
            let counter = parseInt (Math.random()*1000);
            let div ='<div class="row" style="margin-bottom:20px">' +
                '<div class="col-md-5"><select class="select2 select2-without-ajax"' +
                '   name="trainer_ids[\''+counter+'\']"' +
                '   data-value ="_id">' ;
                div+='<option value="">Select trainer</option>';
                @foreach($trainers as $trainer)
                div+='<option value="{{$trainer->_id}}">{{$trainer->name}}</option>';
                @endforeach
                div+= '</select></div>' +
                '<div class="col-md-5"><input type="text" name="trainer_monthly_cost[\''+counter+'\']" required' +
                '   placeholder="cost per month" class="form-control col-md-5"/></div>' +
                '<div class="col-md-2"><div class="btn btn-danger" onClick="$(this).parent().parent().remove()">remove</div></div>' +
                '<div class="col-md-12"><br/><input type="text" placeholder="additional note" class="form-control"' +
                ' name="trainer_notes[\''+counter+'\']" class=""/> </div></div>';
            $('#trainers-box').append(div);
            $('.select2-without-ajax').select2({ width: '100%'});
        }

    </script>
@endpush

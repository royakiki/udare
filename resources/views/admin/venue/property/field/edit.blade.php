@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> Edit Field </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))
                            <li>
                                <a href="{{$route}}">
                                    <i class="fa fa-list"></i>
                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('admin.field.update',['venue'=>$field->venue_id,
                        'field'=>$field->_id])}}"
                              class="form-horizontal form-label-left">
                            @csrf
                            @method('PUT')
                            <div type="button" class="clp">
                                <h3>Properties</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>

                            <div  class="row clpc ">
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="name" name="name" required value="{{$field->name}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="capacity">Capacity</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="capacity" name="capacity" required
                                                   value="{{$field->capacity}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="sport_id">
                                            Sport
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="sport_id"
                                                    id="sport_id"
                                                    data-route="{{route('select.ajax','Sport')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                    <option value="{{$field->sport_id}}" selected>
                                                        {{$field->sport->name ?? ''}}
                                                    </option>
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="status_id">
                                            Status
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="status_id"
                                                    id="status_id"
                                                    data-route="{{route('select.ajax','Status')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                <option value="{{$field->status_id}}" selected>
                                                    {{$field->status->name ??''}}
                                                </option>
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->


                            <div type="button" class="clp">
                                <h3>Field Gallery</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>

                            <div class="row clpc ">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Gallery
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="needsclick dropzone file-uploader" id="dz-documents"
                                             data-route="{{route('admin.field.image-upload',['venue'=>$field->venue_id,'id'=>$field->_id])}}"
                                             data-route-remove="{{route('admin.field.image-remove',['venue'=>$field->venue_id,'id'=>$field->_id])}}"
                                             data-route-get="{{route('admin.field.images',['venue'=>$field->venue_id,'id'=>$field->_id])}}"
                                             data-multiple="true"
                                             data-name="gallery" data-files="">

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div type="button" class="clp">
                                <h3>Slots</h3>

                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>

                            <div class="row clpc ">
                                <button type="button" style="" class="btn btn-round btn-success btn-new"
                                        onclick="window.location.href = '{{ route('admin.slot.create',$field->_id) }}'">New <i
                                        class="fa fa-plus"></i></button>
                                <div class="panel with-nav-tabs panel-default">
                                    <div class="panel-heading">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#monday" data-toggle="tab" onclick="resizeTable()" >Monday</a></li>
                                            <li><a href="#tuesday" data-toggle="tab" onclick="resizeTable()" >Tuesday</a></li>
                                            <li><a href="#wednesday" data-toggle="tab" onclick="resizeTable()" >Wednesday</a></li>
                                            <li><a href="#thursday" data-toggle="tab" onclick="resizeTable()" >Thursday</a></li>
                                            <li><a href="#friday" data-toggle="tab" onclick="resizeTable()" >Friday</a></li>
                                            <li><a href="#saturday" data-toggle="tab" onclick="resizeTable()" >Saturday</a></li>
                                            <li><a href="#sunday" data-toggle="tab" onclick="resizeTable()" >Sunday</a></li>

                                        </ul>
                                    </div>
                                    <div class="panel-body" >
                                        @php($slot = new \App\Models\TimeSlot())
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="monday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                ['fields'=>$slot->fields,
                                                'route'=>route('admin.slot.index',$field->_id).'?day=monday'])
                                            </div>
                                            <div class="tab-pane fade" id="tuesday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=tuesday'])
                                            </div>
                                            <div class="tab-pane fade" id="wednesday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=wednesday'])
                                            </div>
                                            <div class="tab-pane fade" id="thursday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=thursday'])
                                            </div>
                                            <div class="tab-pane fade" id="friday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=friday'])
                                            </div>
                                            <div class="tab-pane fade" id="saturday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=saturday'])
                                            </div>
                                            <div class="tab-pane fade" id="sunday" style="height: 800px">
                                                @include('admin.layouts.datatable.multi-widget',
                                                                                                ['fields'=>$slot->fields,
                                                                                                'route'=>route('admin.slot.index',$field->_id).'?day=sunday'])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Edit
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@include('admin.layouts.datatable.multi-dt-js')
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('vendors/sweet-alerts/sweetalert.min.js')}}"></script>
    <script src="{{ asset('vendors/datatables.net/js/colvis.js') }}"></script>

    <script>
        $(function () {
            function sleep(milliseconds) {
                var start = new Date().getTime();
                for (var i = 0; i < 1e7; i++) {
                    if ((new Date().getTime() - start) > milliseconds) {
                        break;
                    }
                }
            }
        });
    </script>
    @include('admin.layouts.form.javascript')

    <script>

        ajaxSelect('sport_id','Sport')
        ajaxSelect('status_id','Status')
        function resizeTable(){
            setTimeout( function() {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            },500);
        }
    </script>
@endpush

@push('css')
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/sweet-alerts/sweetalert.css')}}" rel="stylesheet">
    {{--    <link href="https://nightly.datatables.net/buttons/css/buttons.dataTables.css?_=c6b24f8a56e04fcee6105a02f4027462.css" rel="stylesheet" type="text/css" />--}}
    <style>
        #table_processing {
            z-index: 100;
            height: 50px;
        }

        .dataTables_filter {
            width: unset;
        }

        .page-title .title_right .pull-right {
            margin: unset;
        }

        .buttons-colvis {
            height: 30px;
        }


    </style>
@endpush


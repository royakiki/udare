<div class="row">
    <div class="col-lg-4">
        <div id="new_slot">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Day</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <select class="select2 select2-slot" id="slot_day">
                        <option value="monday">Monday</option>
                        <option value="tuesday">Tuesday</option>
                        <option value="wednesday">Wednesday</option>
                        <option value="thursday">Thursday</option>
                        <option value="friday">Friday</option>
                        <option value="saturday">Saturday</option>
                        <option value="sunday">Sunday</option>
                    </select>
                    <span id="day-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Start Time</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <input type="time" id="start_time" >
                    <span id="start_time-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">End Time</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <input type="time" id="end_time" >
                    <span id="end_time-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Break in minutes</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <input type="number" id="break_minutes" >
                    <span id="break_minutes-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Price</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <input type="number" id="price" >
                    <span id="price-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Description</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <textarea  id="description" ></textarea>
                    <span id="price-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Expertise Level</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <select class="select2 select2-slot" id="expertise_level">
                        <option value="beginner">Beginner</option>
                        <option value="mid">Med Level</option>
                        <option value="expert">Expert</option>
                    </select>
                    <span id="expertise_level-error"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Selections</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <select class="select2 select2-slot" id="selection">
                        @php($selections = \App\Models\Selection::all())
                        @foreach($selections as $selection)
                            <option id="{{$selection->_id}}">{{$selection->name}}</option>
                        @endforeach
                    </select>
                    <span id="day-error"></span>
                </div>
            </div>
            @if($sport->attendance_type == "multi")
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-12 col-xs-12"
                           for="name">Maximum Booking Limit</label>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <input type="number" id="booking_limit" >
                        <span id="booking_limit-error"></span>
                    </div>
                </div>
            @endif
            @if($sport->attendance_type == "single")
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-12 col-xs-12"
                           for="name">Maximum Allowed Participants</label>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <input type="number" id="max" >
                        <span id="max-error"></span>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-12 col-xs-12"
                       for="name">Minimum Allowed Participants</label>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <input type="number" id="min" >
                    <span id="min-error"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                    <button  class="btn btn-success btn-submit-model">
                        Create
                    </button>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#monday" data-toggle="tab">Monday</a></li>
                    <li><a href="#tuesday" data-toggle="tab">Tuesday</a></li>
                    <li><a href="#wednesday" data-toggle="tab">Wednesday</a></li>
                    <li><a href="#thursday" data-toggle="tab">Thursday</a></li>
                    <li><a href="#friday" data-toggle="tab">Friday</a></li>
                    <li><a href="#saturday" data-toggle="tab">Saturday</a></li>
                    <li><a href="#sunday" data-toggle="tab">Sunday</a></li>

                </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="monday">
                        <div class="time-slot-header">
                            <
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tuesday">

                    </div>
                    <div class="tab-pane fade" id="wednesday">

                    </div>
                    <div class="tab-pane fade" id="thursday">

                    </div>
                    <div class="tab-pane fade" id="friday">

                    </div>
                    <div class="tab-pane fade" id="saturday">

                    </div>
                    <div class="tab-pane fade" id="sunday">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<script>
    $(function () {

        var table{{$udid}} = $('#table{{$udid}}').DataTable({
            fnInitComplete: () => {
                $('#table_processing').css({'height': '50px', 'z-index': '100'});
            },
            processing: true,
            responsive: true,
            fixedHeader: true,
            "order": [[1, "asc"]],
            serverSide: true,
            autoWidth: true,
            paging: true,
            pageLength: 25,
            bFilter: true,
            bLengthChange: true,
            searching: true,
            bSortable: true,
            dom: "Blfrtip",
            scrollY: "500px",
            scrollCollapse: true,
            buttons:
                [
                    'colvis',
                    {
                        extend: "copy", className: "btn-sm", exportOptions: {
                            // columns: [1,2]
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "csv", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "excel", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "pdfHtml5", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "print", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        },
                    }
                ],
            columns:
                [
                    {data: 'DT_RowIndex', sortable: false, orderable: false, searchable: false},
                        @foreach( $fields as  $value)
                    {
                        data: '{{$value['key']}}', name: '{{$value['db_name']}}'
                        @if($value['type'] == 'object')
                        {{--                        , render: function (data, type, row) {console.log(row);--}}
                        {{--                            return "<a href='/users/" + type + "'>" + typeof row  + "</a>"--}}
                        {{--                        }--}}
                        {{--                        @endif--}}
                        , className: 'td-clickable'
                        @endif
                    },
                        @endforeach
                    {
                        data: 'action', sortable: false, orderable: false, searchable: false
                    }
                ],
            lengthMenu:
                [[1, 5, 10, 25, 50, 100], [1, 5, 10, 25, 50, 100]],
            pagingType:
                "simple_numbers",
            language:
                {

                }
            ,

            ajax: {
                url: "{{$route}}",
                data: function (d) {

                }
            }
        });


        $('table.dataTable').on('click', '.btn-delete-model', function (e) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover again !",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (!willDelete) {
                    return false;
                } else {
                    var _token = $(this).parent().find('input[name="_token"]').val();
                    var route = $(this).parent().attr('action');
                    $.ajax({
                        url: route,
                        type: "POST",
                        data: {
                            _token: _token,
                            _method: 'DELETE'
                        },
                        success: function () {
                            swal(
                                'Deleted!',
                                'Successfully',
                                'success'
                            );
                            table./*api().*/ajax.reload();
                        }
                    });
                }
            });
        });

    });
</script>

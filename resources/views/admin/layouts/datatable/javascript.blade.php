
<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-select/js/dataTables.select.min.js')}}"></script>

<script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('vendors/sweet-alerts/sweetalert.min.js')}}"></script>
<script src="{{ asset('vendors/datatables.net/js/colvis.js') }}"></script>


<script>
    $(function () {
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }

        function getCompanyInfo(key, def) {

        }

        // $.fn.dataTable.ext.search.push(
        //     function( settings, data, dataIndex ) {
        //         alert('inside push function');
        //         var service = $('#test').val();
        //         var age = parseFloat( data[5] ) || 0; // use data for the age column
        //
        //         if (service.toLowerCase() === age.toLowerCase())
        //         {
        //             alert('i am inside');
        //             return true;
        //         }
        //         alert('i am outside');
        //         return false;
        //     });
        var table = $('#table').DataTable({
            fnInitComplete: () => {
                $('#table_processing').css({'height': '50px', 'z-index': '100'});
            },
            processing: true,
            responsive: true,
            fixedHeader: true,
            "order": [[1, "asc"]],
            serverSide: true,
            autoWidth: true,
            paging: true,
            pageLength: 25,
            bFilter: true,
            bLengthChange: true,
            searching: true,
            bSortable: true,
            dom: "Blfrtip",
            scrollY: "500px",
            scrollCollapse: true,
            select:true,
            buttons:
                [
                    'colvis',
                    {
                        extend: "copy", className: "btn-sm", exportOptions: {
                            // columns: [1,2]
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "csv", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "excel", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "pdfHtml5", className: "btn-sm", exportOptions: {
                            columns: "th:not(:last-child)"
                        }
                    },
                    {
                        extend: "print", className: "btn-sm", exportOptions: {

                        },

                    }
                ],
            columns:
                [
                    // {
                    //     data: 'DT_RowIndex', width: "10%", sortable: false, orderable: false, searchable: false
                    // },
                    {
                        targets: 0,
                        data: null,
                        defaultContent: '',
                        orderable: false,
                        className: 'select-checkbox'
                    },
                        @foreach( $fields as  $value)
                    {
                        data: '{{$value['key']}}', name: '{{$value['db_name']}}'
                        @if($value['type'] == 'object')
                        {{--                        , render: function (data, type, row) {console.log(row);--}}
                        {{--                            return "<a href='/users/" + type + "'>" + typeof row  + "</a>"--}}
                        {{--                        }--}}
                        {{--                        @endif--}}
                        , className: 'td-clickable'
                        @endif
                    },
                        @endforeach
                    {
                        data: 'action', width: "30%", sortable: false, orderable: false, searchable: false
                    }
                ],
            lengthMenu:
                [[1, 5, 10, 25, 50, 100], [1, 5, 10, 25, 50, 100]],
            pagingType:
                "simple_numbers",
            language:
                {
                    paginate: {
                        previous: "<i class='fa fa-angle-left'></i>",
                        next: "<i class='fa fa-angle-right'></i>"
                    }
                    // "processing": "***"
                }
            ,
            // oLanguage: {sProcessing: "<div id='overlay' style='margin-bottom:30px !important;color:red !important;position:fixed'>Processing...</div>"},
            // aoColumnDefs: [
            //     {'bSortable': false, 'aTargets': [0]},
            //     {'bSearchable': false, 'aTargets': [0]},
            // ],
            ajax: {
                url: "{{$route}}",
                data: function (d) {
                    var reservation_time = $('#reservation-time');
                    if (typeof reservation_time.val() != "undefined") {
                        let datevals = reservation_time.val().split(' - ');
                        d.from = datevals[0].trim();
                        d.to = datevals[1].trim();
                    }
                }
            }
        });


        // $('#test').keyup( function() {
        //     table.draw();
        // } );

        if (typeof $('#reservation-time').val() != "undefined") {
            $('#btn-date-search').on('click', function () {
                table/*api()*/.ajax.reload(); //( null, false ); // user paging is not reset on reload
            });
            // $('#reservation-time').on('focus', function () {
            //     $('.daterangepicker').attr('style', 'top: 112px;left: auto;display: block;right: 34%');
            //
            // }); later for setting the pace og the daterangepicker
        }

        $('table.dataTable').on('click', '.btn-delete-model', function (e) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover again !",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (!willDelete) {
                    return false;
                } else {
                    var _token = $(this).parent().find('input[name="_token"]').val();
                    var route = $(this).parent().attr('action');
                    $.ajax({
                        url: route,
                        type: "POST",
                        data: {
                            _token: _token,
                            _method: 'DELETE'
                        },
                        success: function () {
                            swal(
                                'Deleted!',
                                'Successfully',
                                'success'
                            );
                            table./*api().*/ajax.reload();
                        }
                    });
                }
            });
        });

        // $('a.toggle-vis').on('click', function (e) {
        //     e.preventDefault();
        //
        //     // Get the column API object
        //     var column = table.column($(this).attr('data-column'));
        //
        //     // Toggle the visibility
        //     column.visible(!column.visible());
        // });
    });
</script>

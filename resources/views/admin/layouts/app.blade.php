<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="" type="image/ico"/>

    <title>@yield('title','')</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors\bootstrap\dist\css\bootstrap.min.css')}}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
{{--    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">--}}
    <!-- iCheck -->
{{--    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">--}}

    <!-- bootstrap-progressbar -->
{{--    <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">--}}
    <!-- JQVMap -->
{{--    <link href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>--}}
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
{{--    <link href="{{asset('vendors/select2/dist/css/select2.css')}}" rel="stylesheet">--}}
    <link href="{{ asset('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

    <!-- Custom Theme Style -->
    <link href="{{asset('css/venue-owner/custom2.css')}}" rel="stylesheet">
    @toastr_css
    <style>
        #overlay {
            position: fixed;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            z-index: 5000000;
            height: 106%;
            width: 100%;
            text-align: center;
            background: #E7E7EB;
            vertical-align: middle;
        }

        /*#overlay img {*/
        /*    height: 20%;*/
        /*    width: 15%;*/
        /*}*/

        #cal-icon {
            border-radius: 25px 0px 0px 25px;
        }

        table.dataTable thead th:first-child .sorting_asc:after,
        table.dataTable thead th:first-child .sorting_desc:after {
            content: unset !important;
        }

        table.dataTable th:first-child thead th:first-child .sorting_asc,
        table.dataTable th:first-child thead th:first-child .sorting_desc {
            width: 20px !important;
        }

        #leftCol {
            position: fixed;
            width: 150px;
            overflow-y: scroll;
            overflow-x: hidden;
            top: 0;
            bottom: 0;
        }
    </style>
    @stack('css')
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
{{--        <div id="overlay">--}}
{{--            <img src="{{ asset('img/dr.gif') }}">--}}
{{--        </div>iv id="overlay">--}}
{{--            <img src="{{ asset('img/dr.gif') }}">--}}
{{--        </div>--}}
        {{--header with side bar--}}
        @include('admin.layouts.header')
        <div class="right_col menu" role="main">
            @yield('content')
        </div>
        @include('admin.layouts.footer')
    </div>
</div>
<!-- jQuery -->

<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>


<!-- Bootstrap -->
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>


<!-- FastClick -->
{{--<script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>--}}
<!-- NProgress -->
{{--<script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>--}}
<!-- Chart.js -->
{{--<script src="{{ asset('vendors/moment/src/moment.js') }}"></script>--}}
{{--<script src="{{asset('vendors/Chart.js/dist/Chart.min.js')}}"></script>--}}
<!-- gauge.js -->
{{--<script src="{{asset('vendors/gauge.js/dist/gauge.min.js')}}"></script>--}}
<!-- bootstrap-progressbar -->
{{--<script src="{{asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>--}}
<!-- iCheck -->
{{--<script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>--}}
<!-- Skycons -->
{{--<script src="{{asset('vendors/skycons/skycons.js')}}"></script>--}}
<!-- Flot -->
{{--<script src="{{asset('vendors/Flot/jquery.flot.js')}}"></script>--}}
{{--<script src="{{asset('vendors/Flot/jquery.flot.pie.js')}}"></script>--}}
{{--<script src="{{asset('vendors/Flot/jquery.flot.time.js')}}"></script>--}}
{{--<script src="{{asset('vendors/Flot/jquery.flot.stack.js')}}"></script>--}}
{{--<script src="{{asset('vendors/Flot/jquery.flot.resize.js')}}"></script>--}}
<!-- Flot plugins -->
{{--<script src="{{asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>--}}
{{--<script src="{{asset('vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>--}}
{{--<script src="{{asset('vendors/flot.curvedlines/curvedLines.js')}}"></script>--}}
<!-- DateJS -->
<script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
<!-- JQVMap -->
{{--<script src="{{asset('vendors/jqvmap/dist/jquery.vmap.js')}}"></script>--}}
{{--<script src="{{asset('vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>--}}
{{--<script src="{{asset('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>--}}
<!-- bootstrap-daterangepicker -->
{{--<script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>--}}
{{--<script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>--}}
{{--<script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="{{asset('js/toastr.js')}}"></script>


<script>

    $(function () {
        $('.select2-without-ajax').select2({ width: '100%'});


        $("a[data-original-title='FullScreen']").on('click', function () {
            toggleFullScreen();
        });

        function toggleFullScreen() {
            document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement
                ? document.exitFullscreen
                ? document.exitFullscreen()
                : document.msExitFullscreen
                    ? document.msExitFullscreen()
                    : document.mozCancelFullScreen
                        ? document.mozCancelFullScreen()
                        : document.webkitExitFullscreen && document.webkitExitFullscreen()
                : document.documentElement.requestFullscreen
                ? document.documentElement.requestFullscreen()
                : document.documentElement.msRequestFullscreen
                    ? document.documentElement.msRequestFullscreen()
                    : document.documentElement.mozRequestFullScreen
                        ? document.documentElement.mozRequestFullScreen()
                        : document.documentElement.webkitRequestFullscreen && document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)
        }

        window.reset = function (e) {
            e.wrap('<form>').closest('form').get(0).reset();
            e.unwrap();
        };

    });

    var coll = document.getElementsByClassName("clp");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
            try{
                setTimeout( function() {
                    $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
                },500);

                var oSettings = $($.fn.dataTable.tables(true)).DataTable().fnSettings();
                oSettings.oScroll.sY = '600px';
                $($.fn.dataTable.tables(true)).DataTable().fnDraw();
            }catch (e) {

            }

        });
    }
</script>


@stack('js')

<!-- Custom Theme Scripts -->
<script src="{{asset('js/custom.min.js')}}"></script>
</body>
@toastr_js
@toastr_render
</html>

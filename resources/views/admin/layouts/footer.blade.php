<!-- footer content -->
<footer>
    <div class="pull-right">
        @lang('app.copyright')
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->

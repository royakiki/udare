<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" >
            <a href="{{ route('dashboard') }}" class="site_title">
                <img src="{{asset('img/udare-sidebar-logo.png')}}" alt="uDare" />
            </a>
        </div>

        <div class="clearfix"></div>

        {{--        <!-- menu profile quick info -->--}}
        {{--        <div class="profile clearfix">--}}
        {{--            <div class="profile_pic">--}}
        {{--                <img src="images/img.jpg" alt="..." class="img-circle profile_img">--}}
        {{--            </div>--}}
        {{--            <div class="profile_info">--}}
        {{--                <span>Welcome,</span>--}}
        {{--                <h2>John Doe</h2>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <!-- /menu profile quick info -->--}}

        <br/>

    @include('admin.layouts.sidebar')

    <!-- /menu footer buttons -->
{{--        <div class="sidebar-footer hidden-small">--}}
{{--            --}}{{--            <a data-toggle="tooltip" data-placement="top" title="Settings">--}}
{{--            --}}{{--                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>--}}
{{--            --}}{{--            </a>--}}
{{--            <a data-toggle="tooltip" data-placement="top" title="FullScreen">--}}
{{--                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>--}}
{{--            </a>--}}
{{--            --}}{{--            <a data-toggle="tooltip" data-placement="top" title="Lock">--}}
{{--            --}}{{--                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>--}}
{{--            --}}{{--            </a>--}}
{{--            <a>--}}
{{--                <form action="{{route('logout')}}" method="POST">--}}
{{--                    @csrf--}}
{{--                    <button type="submit" style="background:inherit;border: inherit;font-size: inherit"--}}
{{--                            data-toggle="tooltip"--}}
{{--                            data-placement="top" title="Logout">--}}
{{--                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>--}}
{{--                    </button>--}}
{{--                </form>--}}
{{--            </a>--}}
{{--        </div>--}}
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="pull-right" id="profile">
        <a style="margin-right: 10px;vertical-align: middle">{{auth()->user()->name}}</a>
        <i class="fa fa-user"></i>
    </div>
    <div class="pull-right" id="settings">
        <i class="fa fa-bell" style="margin-right: 15px;"></i>
        <i class="fa fa-gear" style="margin-right: 5px"></i>
    </div>
</div>
<!-- /top navigation -->


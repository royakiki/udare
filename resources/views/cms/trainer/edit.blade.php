@extends('cms.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))
                            <li>
                                <a href="{{$route}}">
                                    <i class="fa fa-list"></i>
                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif
                                </a>
                            </li>
                        @endif
                    </ul><!-- /.nav -->
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h3>Overview</h3>
                        <form method="POST" action="{{route('trainer.update', $trainer->_id)}}"
                              class="form-horizontal form-label-left">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Venue Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="venue_name" name="name" required
                                                   value="{{$venue->name}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="commercial_name">
                                            Official Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="commercial_name" name="commercial_name" required
                                                   value="{{$venue->commercial_name}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="email">
                                            Email Id
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="email" name="email" required
                                                   value="{{$venue->email}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="owner_id">
                                            Owner
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="owner_id"
                                                    id="owner_id"
                                                    data-route="{{route('select.ajax','User')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    required>
                                                <option value="{{$venue->owner['_id']}}"
                                                        selected>{{$venue->owner['name']}}</option>
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="country_id">
                                            Country
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="country_id"
                                                    id="country_id"
                                                    data-route="{{route('select.ajax','Country')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    onchange="ajaxSelect('location_id','Location', 'country_id='+ this.value)"
                                                    required>
                                                <option value="{{$venue->country['_id']}}"
                                                        selected>{{$venue->country['name']}}</option>
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="location_id">
                                            Location
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="location_ids"
                                                    id="location_id"
                                                    data-route="{{route('select.ajax','Location')}}"
                                                    data-name="name"
                                                    data-value="_id" multiple required>
                                                @foreach( $venue->locations as $location)
                                                    <option value="{{$location->_id}}"
                                                            selected>{{$location->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="phone">
                                            Contact Number
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="phone" name="phone" required
                                                   value="{{$venue->phone}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="selection_ids">
                                            Select Sections
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="selection_ids[]"
                                                    id="selection_ids"
                                                    data-route="{{route('select.ajax','Selection')}}"
                                                    data-name="name"
                                                    data-value="_id" multiple required>
                                                @if($venue->selections != null )
                                                    @foreach( $venue->selections as $selection)
                                                        <option value="{{$selection->_id}}"
                                                                selected>{{$selection->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="status_id">
                                            Select Status
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="status_id"
                                                    id="status_id"
                                                    data-route="{{route('select.ajax','Status')}}"
                                                    data-name="name"
                                                    data-value="_id" required>
                                                @if ($venue->status != null )
                                                    <option value="{{$venue->status->_id}}"
                                                            selected>{{$venue->status->name}}</option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <h3>Sports & Categories</h3>
                            <hr class="rounded" style="background-color: #E6E9ED">
                            <div class="row">
                                @foreach( $categories as $category)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="{{$category->_id}}">
                                                {{$category->name}}
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2 select2-ajax"
                                                        name="sport_ids[]"
                                                        id="{{$category->_id}}"
                                                        data-route="{{route('select.ajax','Sport')}}?parent_id={{$category->_id}}"
                                                        data-name="{{$category->name}}"
                                                        multiple="multiple">
                                                    @if($venue->sports != null)
                                                        @foreach($venue->sports as $sport)
                                                            @if ( $sport->parent_id == $category->_id)
                                                                <option selected value="{{$sport->id}}">
                                                                    {{$sport->name}}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- /.col-* -->
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                @endforeach
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="other_sport">
                                            uOthers
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2 select2-ajax-add"
                                                    name="other_sport_names[]"
                                                    id="other_sport"
                                                    multiple="multiple">
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->


                            <h3>Properties</h3>
                            <hr class="rounded" style="background-color: #E6E9ED">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <a href="{{route('field.index',$venue->_id)}}">
                                        <div class="btn btn-primary venue-btn">Fields</div>
                                    </a>
                                </div><!-- /.col-* -->
                                <div class="col-md-4">
                                    <button class="btn btn-primary venue-btn">Locations</button>
                                </div><!-- /.col-* -->
                                <div class="col-md-4">
                                    <button class="btn btn-primary venue-btn">Trainers</button>
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->


                            <h3>Functionalities</h3>
                            <hr class="rounded" style="background-color: #E6E9ED">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <button class="btn btn-primary venue-btn"> Memberships</button>
                                </div><!-- /.col-* -->
                                <div class="col-md-4">
                                    <button class="btn btn-primary venue-btn"> Sessions</button>
                                </div><!-- /.col-* -->
                                <div class="col-md-4">
                                    <button class="btn btn-primary venue-btn"> Matches</button>
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit"
                                            class="btn btn-success btn-submit-model">Edit
                                    </button>
                                </div><!-- col-* -->
                            </div><!-- /.form-group -->

                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection

@push('js')
    @include('cms.layouts.form.javascript')
    <script>

        ajaxSelect('country_id', 'Country')
        ajaxSelect('location_id', 'Location','country_id={{$venue->country_id}}')
        ajaxSelect('selection_ids','Selections')
        ajaxSelect('status_id','Status')
        ajaxSelect('owner_id','Owner')

        @foreach( $categories as $category)
        ajaxSelect('{{$category->_id}}','Category')
        @endforeach

        $(".select2-ajax-add").select2({
            tags: true,
            width: '100%',
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    </script>
@endpush

<script>
    $(function () {

        var k = "";

        function rgb2hex(rgb) {
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
        }

        $('.btn-submit-model').on('click', function (e) {
            if (typeof $('.colorpicker').val() !== "undefined") {
                if ($('.colorpicker').val().indexOf('#') == -1) {
                    var hex = rgb2hex($('.colorpicker').val());
                    $('.colorpicker').val(hex);
                }
            }
            if (typeof $('#hdn').val() !== "undefined")
                $('#hdn').val(k);

        });
    });

    function ajaxSelect(id,title, query = null) {
        let route = $('#' + id).attr('data-route');
        let name = $('#' + id).attr('data-name');
        let value = $('#' + id).attr('data-value');
        if ($('#' + id).attr('data-flag') !== undefined) {
            let flag = $('#' + id).attr('data-search');
            route = route + '?search=' + flag;
        }
        if (query != null) {
            route += route.includes("?") ? ("&" + query) : ("?" + query)
        }
        $('#' + id).select2({
            width: '100%',
            placeholder: title,
            allowClear: true,
            ajax: {
                url: route,
                dataType: 'json',
                processResults: function (data) {
                    results = [];

                    // results.push({id: "", text:name})

                    data = $.map(data, function (obj) {
                        return {id: obj[value], text: obj[name]};
                    })
                    $.each(data, function (key, value) {
                        results.push(value)
                    })
                    return {
                        results: results
                    };
                },
                error: function (data) {
                    console.log(data)
                },
                cache: true
            }
        });

    }
</script>
<script>
    Dropzone.autoDiscover = false;
    $('.file-uploader').each(function(key, value){
        let route = $(value).attr('data-route');
        let divname = $(value).attr('data-name');
        let initFiles = $(value).attr('data-files');
        let multiple = $(value).attr('data-multiple');
        var uploadedDocumentMap = {}
        var options = Dropzone.options.documentDropzone = {
            url: route,
            maxFilesize: 20, // MB
            maxFiles: multiple?100:1,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="'+divname+(multiple?'[]':'')+'" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('form').find('input[name="'+divname+'[]"][value="' + name + '"]').remove()
            },
            init: function () {
                var files = initFiles.split(',');
                    for (var i in files) {

                        var mock = files[i];
                        mock.accepted = true;

                        this.files.push(mock);
                        this.emit('addedfile', mock);
                        this.createThumbnailFromUrl(mock, mock);
                        this.emit('complete', mock);

                    var file = files[i]
                    this.options.addedfile.call(this, "{{env("APP_URL")}}/"+file)
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="'+divname+'[]" value="' + file.file_name + '">')
                }
            }
        }

        var myDropzone = new Dropzone(value,options );
    });
    function preview_image(event,identifier) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('image-holder'+identifier);
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>

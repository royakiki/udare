<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('css/venue-owner/custom.css') }}" rel="stylesheet">
    <style>
        #first-image {
            background-image: url('{{asset('img/WB-1.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }

        #second-image {
            background-image: url('{{asset('img/WB-2.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }

        #third-image {
            background-image: url('{{asset('img/WB-3.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
        @media screen and (max-width: 1440px){
            .carousel-item {
                height: 700px;
            }
        }


        body {
            background: white;
        }

        .navbar-toggler {
            border-color: #8BD8C6 !important;
        }

        .navbar-toggler-icon {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'%3e%3cpath stroke='rgba(63,65,219, 1)' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e") !important;
        }
    </style>
    <link href="{{asset('vendors/bootstrap/less/responsive-utilities.less')}}" rel="stylesheet">
    @toastr_css
</head>

<body data-spy="scroll" data-target="#navbar">

<nav class="navbar navbar-expand-md fixed-top navbar-light" id="navbar">
    <div class="container-fluid nav-container ubuntu-medium">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('img/uDareLogo_web.png')}}" class="logo" alt="UDare"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a data-scroll class="nav-link active" href="#home">HOME</a>
                </li>
                <li class="nav-item">
                    <a data-scroll class="nav-link" href="#solution">SOLUTION</a>
                </li>
                <li class="nav-item">
                    <a data-scroll class="nav-link" href="#features">FEATURES</a>
                </li>
                <li class="nav-item">
                    <a data-scroll class="nav-link" href="#about">ABOUT</a>
                </li>
                <li class="nav-item">
                    <a data-scroll class="nav-link" href="#contact">CONTACT</a>
                </li>
                @guest
                    <li class="nav-item">
                        <a href="{{ route('login') }}">
                            <button class="btn venue-login-btn d-none d-md-block ubuntu-light">Register / Log in
                            </button>
                            <a class="nav-link d-block d-md-none" href="{{route('login')}}">Register / Log in</a>
                        </a>
                    </li>
                @else
                @endguest
            </ul><!-- /.navbar-nav -->
        </div><!-- /.collapse -->
    </div><!-- /.container-fluid .nav-container -->
</nav>

<div id="home">
    <div class="container-fluid">
        <div id="carousel" class="carousel slide position-absolute w-100" data-ride="carousel">
            <ol class="carousel-indicators">
                <li class="carousel-indicator active" data-target="#carousel" data-slide-to="0">
                    <hr class="vertical-line white">
                </li>
                <li class="carousel-indicator" data-target="#carousel" data-slide-to="1">
                    <hr class="vertical-line white">
                </li>
                <li class="carousel-indicator" data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active" id="first-image">
                    <img class="d-block w-100" src="{{asset('img/WB-1.png')}}" style="visibility: hidden">
                </div>
                <div class="carousel-item" id="second-image">
                    <img class="d-block w-100" src="{{asset('img/WB-1.png')}}" style="visibility: hidden">
                </div>
                <div class="carousel-item" id="third-image">
                    <img class="d-block w-100" src="{{asset('img/WB-1.png')}}" style="visibility: hidden">
                </div>
            </div>
        </div><!-- /#carousel .carousel -->

        <div class="row" style="padding-top: 13.2%;padding-left: 2.2%;padding-right: 3.9%;">
            <div class="col-md-5 d-none d-lg-block">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title orbitron-regular">WE MATCH. YOU PLAY.</h2>
                        <p class="card-text ubuntu-regular">uDare connects you to world's on demand sports and fitness
                            exercises.</p>
                        <div class="row" style="margin-top: 22%">
                            <div class="col-sm-12 col-lg-6">
                                <a href="#!">
                                    <div class="btn-group" role="group" aria-label="Google Play">
                                        <button class="btn google-play-btn-left">
                                            <img height="30" src="{{asset('img/google-play-logo.png')}}"
                                                 alt="google play"/>
                                        </button>
                                        <button class="btn google-play-btn-right ubuntu-light">
                                            Download it from <br> <b class="ubuntu-regular">GOOGLE PLAY</b>
                                        </button>
                                    </div><!-- /.btn-group -->
                                </a>
                            </div><!-- /.col-* -->
                            <div class="col-sm-12 col-lg-6">
                                <a href="#!">
                                    <div class="btn-group" role="group" aria-label="App Store">
                                        <button class="btn app-store-btn-left">
                                            <img height="30" src="{{asset('img/apple-logo.png')}}" alt="apple store"/>
                                        </button>
                                        <button class="btn app-store-btn-right ubuntu-light">
                                            Download it from <br> <b class="ubuntu-regular">APP STORE</b>
                                        </button>
                                    </div><!-- /.btn-group -->
                                </a>
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.card-body -->
                </div><!-- /.card -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->

    </div><!-- /.container-fluid -->
</div><!-- /#home -->


<div class="d-block d-md-none d-lg-none " style="background-color: #EDFEFA;padding: 10px;text-align: center">
    <div class="card-body">
        <h2 class="card-title orbitron-regular">WE MATCH. YOU PLAY.</h2>
        <p class="card-text ubuntu-regular">uDare connects you to world's on demand sports and fitness exercises.</p>
        <div class="row store-buttons">
            <div class="col-6">
                <a href="#!">
                    <div class="btn-group" role="group" aria-label="Google Play">
                        <button class="btn google-play-btn-left">
                            <img height="30" src="{{asset('img/google-play-logo.png')}}"
                                 alt="google play"/>
                        </button>
                        <button class="btn google-play-btn-right ubuntu-light">
                            Download it from <br> <b class="ubuntu-regular">GOOGLE PLAY</b>
                        </button>
                    </div><!-- /.btn-group -->
                </a>
            </div><!-- /.col-* -->
            <div class="col-6">
                <a href="#!">
                    <div class="btn-group" role="group" aria-label="App Store">
                        <button class="btn app-store-btn-left">
                            <img height="30" src="{{asset('img/apple-logo.png')}}" alt="apple store"/>
                        </button>
                        <button class="btn app-store-btn-right ubuntu-light">
                            Download it from <br> <b class="ubuntu-regular">APP STORE</b>
                        </button>
                    </div><!-- /.btn-group -->
                </a>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.card-body -->
</div>


<div id="solution">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 text-center d-none d-lg-block">
                <img class="img-thumbnail" src="{{asset('img/solution.png')}}" alt=""/>
            </div>
            <div class="col-lg-7 p-lg-3">
                <h5 class="ubuntu-light">USMP Solution </h5>
                <h2 class="ubuntu-bold">Ubiquitous-Sports-Matching-PLatForm</h2><br><br>
                <p class="ubuntu-regular">Do you still use online search engines, make phone calls, and pay door to door
                    visits to obtain information and select your best suited Sports Venues. Or are your struggling to
                    find the right fit playpals nearby to exercise your favorite sports?</p>
                <p class="ubuntu-regular">uDare is an online <i>Ubiquitous-Sports-Matching-PIatform (USMP)</i> solution
                    provider for iOS & Android mobile devices that adds value to your daily life by helping to find,
                    book, and exercise your sports whenever, wherever, and with whomever you choose to.</p>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="container-fluid p-md-5" id="solution-second-section">
        <div class="row">
            <div class="col-lg-2 mt-5 text-center">
                <img src="{{asset('img/feature-image-placeholder.png')}}" id="t" class="rounded-circle w-75"
                     alt="solution">
            </div>
            <div class="col-lg-10 mt-5">
                <h5 class="title ubuntu-bold">Users-to-Users</h5>
                <p class="text mt-4 ubuntu-regular">We connect Users-to-Users for you to find right fit playpals with
                    common sports interests.<br>There is nothing more exciting than to meet new friends to exercise your
                    sports to boost the healthy synergy together.</p>
            </div>
            <div class="col-lg-2 mt-5 d-block d-lg-none text-center">
                <img src="{{asset('img/feature-image-placeholder.png')}}" class="rounded-circle w-75" alt="solution">
            </div>
            <div class="col-lg-9 col-sm-12 ml-5 mt-5">
                <h5 class="title ubuntu-bold">Users-to-Venues</h5>
                <p class="text mt-4 ubuntu-regular">We connect Users-to-Venues for you to exercise in best suited venues.
                    There are venues out there that meets your needs at its best - you can simply explore and discover
                    through our mobile application.</p>
            </div>
            <div class="col-lg-2 mt-5 d-none d-lg-block text-center">
                <img src="{{asset('img/feature-image-placeholder.png')}}" id="t2" class="rounded-circle w-75"
                     alt="solution">
            </div>
            <div class="col-lg-2 mt-5 text-center">
                <img src="{{asset('img/feature-image-placeholder.png')}}" class="rounded-circle w-75" alt="solution">
            </div>
            <div class="col-lg-10 mt-5">
                <h5 class="title ubuntu-bold">Venues-to-Communities</h5>
                <p class="text mt-4 ubuntu-regular">We connect Venues-to-Communities for you to be engaged in your
                    communities with sports. Your every drop of sweat counts to help in bringing healthy foundation and
                    sustainable eco-system in your involved communities.</p>
            </div>
        </div><!-- /.row -->
    </div><!-- /#solution-second-section .container-fluid -->

</div><!-- /#solution -->

<div class="container-fluid" id="features">
    <div class="row">
        <div class="col-lg-2 col-sm-2 phone">
            <h3 class="pl-5 pb-4 ubuntu-regular">FEATURES</h3>
            <img class="img-thumbnail" src="{{asset('img/phone.png')}}" alt=""/>
        </div><!-- /.col-* -->
        <div class="col-lg-8 offset-lg-2 col-sm-9 p-lg-5 mt-lg-5 mt-md-4">
            <br><br>
            <p>
            <h2 class="ubuntu-bold_italic mb-4">uMatch <span class="ubuntu-bold">: We Match. You Play.</span></h2>

            <p>
                Willing to quickly book and join your favorite sports match or session? We got you.
                <br>
                Our state-of-the-art sports matching technology will introduce you to loads of them through our uMatch
                feature, and all you need is a simple swipe to explore!
            </p>
            <br>
        </div><!-- /.col-* -->
    </div><!-- /.row -->

    <div class="row feature pb-5">
        <div class="col-lg-8 offset-lg-4 p-md-5">
            <br>
            <h2 class="ubuntu-bold mb-4">Discover Sports Venues Nearby</h2>
            <p>
                Still struggling to find your best suited Sports Venues?
                <br>
                Explore and find Sports Venues nearby that suits your needs and follow them for updates, promotions, and
                events.
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->
    <div class="row feature pb-5">
        <div class="col-lg-8 offset-lg-4 p-md-5">
            <br>
            <h2 class="ubuntu-bold mb-4">Invite Friends &amp Playpals</h2>
            <p>
                Whether or not you need a double or multiple players, or you're new to the hood not yet having friends,
                or you simply do not have enough friends with common sports interest - our feature to invite friends and
                playpals nearby makes it all possible!
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->
    <div class="row feature pb-5">
        <div class="col-lg-8 offset-lg-4 p-md-5">
            <br>
            <h2 class="ubuntu-bold mb-4">Skill Levels &amp Attendance- Checked</h2>
            <p>
                Worried about competing in your skill level and no-shows of playpals?
                <br>
                Our rating feature for skill levels and attendance check upon completion of every matches and sessions
                will push everyone further to be engaged.
                <br>
                <br>
                We all enjoy a little bit of friendly competition!
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->
    <div class="row feature pb-5">
        <div class="col-lg-8 offset-lg-4 p-md-5">
            <br>
            <h3 class="ubuntu-bold mb-4">All-In-One <span class="ubuntu-bold_italic">My Schedule</span></h3>
            <p>
                Easily track and manage all your bookings and schedule in <i>My Schedule</i> Feature
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->
    <div class="row feature pb-5">
        <div class="col-lg-8 offset-lg-4 p-md-5">
            <br>
            <h2 class="ubuntu-bold mb-4">Stay Tuned-In via <span class="ubuntu-bold_italic">Sports Feed</span></h2>

            <p>
                Get connected and stay updated by becoming friends with other Users and following
                Sports Venues through <i>Sports Feed</i>feature. Posting photos/videos, tagging friends and
                Sports Venues will enhance your life to evolve around sports!
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->

</div><!-- /#features .container-fluid -->

<div id="about">

    <div class="container-fluid pl-5 pr-5">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="ubuntu-regular" style="color: #1D2733">ABOUT US</h4>
                <br>
                <p class="ubuntu-light" style="color:#61676F;">At uDare, a company established in South Korea by 3
                    sports enthusiasts, we believe in boosting sports and fitness user experience through our digital
                    technology. We offer our products and services not only to increase profit and reduce cost, but
                    strive to upgrade the entire sports and fitness experience for our Users by entirely transforming
                    the conventional ways of exercising sports.</p>
            </div>
        </div>
        <div class="row about-list-items">
            <div class="col-md-12">
                <h4 class="ubuntu-bold" style="color: #1D2733">Our Just Cause</h4>
            </div>
            <div class="col-md-12">
                <p class="mt-3 ubuntu-light">We aim to create healthy and sound values, both in mental
                    and physical aspects of everyone's daily-life, and finally
                    bring collective well-being culture globally.
                </p>
            </div>
        </div>
        <div class="row about-list-items">
            <div class="col-lg-12">
                <h4 class="ubuntu-bold" style="color: #1D2733">Goal</h4>
            </div>
            <div class="col-lg-12">
                <p class="mt-3 ubuntu-light">We provide Offline-To-Online (020) Platform for Sports Venues and Users to
                    connect,
                    socialize, and
                    exercise together through our user-friendly and game-changing features. We not only seek to connect
                    our
                    Users with sports in every context but work closely with our Sports Venues to empower a sustainable
                    eco-system, and maximize ROI in terms of marketing spend channel.
                </p>
            </div>
        </div>
    </div><!-- /#about .container-fluid -->

    <div class="container-fluid pl-5 pr-5" id="values">
        <h4 class="ubuntu-bold" style="color: #1D2733">Our True Values</h4>
        <div class="row">

            <div class="col-lg-6 value p-sm-5">
                <div class="row text-center">
                    <div class="col-md-12">
                        <img width="180" height="180" class="rounded-circle"
                             src="{{asset('img/feature-image-placeholder.png')}}" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="card-body">
                            <h4 class="card-title ubuntu-bold">Ownership</h4>
                            <p class="card-text text-center pl-md-5 pr-md-5 ubuntu-regular">
                                We see ourselves as a Team Captain. <br>
                                We listen to every feedback, take every problem as
                                our
                                own, stand up to challenges, and take accountability to drive positive change.
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-* -->

            <div class="col-lg-6 value p-sm-5">
                <div class="row text-center">
                    <div class="col-md-12">
                        <img width="180" height="180" src="{{asset('img/feature-image-placeholder.png')}}"
                             class="rounded-circle" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="card-body">
                            <h4 class="card-title ubuntu-bold">Equality</h4>
                            <p class="card-text text-center pl-md-5 pr-md-5 ubuntu-regular">
                                We take equality seriously, whether it is racial, gender, or social status. We pursue
                                progress toward ideal world of everyone being truly equal.
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-* -->

            <div class="col-lg-6 value p-sm-5">
                <div class="row text-center">
                    <div class="col-md-12">
                        <img width="180" height="180" src="{{asset('img/feature-image-placeholder.png')}}"
                             class="rounded-circle" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="card-body">
                            <h4 class="card-title ubuntu-bold">Respect</h4>
                            <p class="card-text text-center pl-md-5 pr-md-5 ubuntu-regular">
                                We believe in respect in-and-out of the pitch, as it is the foundation of the game. We
                                treat
                                all of our Team Members with respect as a part of family, including our external
                                stakeholders
                            </p>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-* -->

            <div class="col-lg-6 value p-sm-5">
                <div class="row text-center">
                    <div class="col-md-12">
                        <img width="180" height="180" src="{{asset('img/feature-image-placeholder.png')}}"
                             class="rounded-circle" alt="">
                    </div>
                    <div class="col-md-12">
                        <div class="card-body">
                            <h4 class="card-title ubuntu-bold">Fair Play</h4>
                            <p class="card-text text-center pl-md-5 pr-md-5 ubuntu-regular">
                                We promote our business with fair play. We seek to accomplish transparency and win with
                                true honesty and integrity.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /#values .container-fluid -->

    <div class="container-fluid p-lg-5" id="team">
        <h3 class="ubuntu-bold" style="color:#1D2733;">Founded By</h3>
        <div class="row mt-5">

            <div class="col-md-4">
                <div class="card">
                    <img class="rounded-circle w-100" src="{{asset('img/fateh.png')}}" alt="Fateh Abu Hijleh">
                </div>
                <div class="team-member text-center">
                    <div class="team-member-header">
                        <h4 class="name ubuntu-bold">Fateh Abu Hijleh</h4>
                        <h4 class="position ubuntu-light">CTO</h4>
                    </div>
                </div>
                <div class="biography text-center pl-3 pr-3">
                    <p class="ubuntu-regular">
                        Fateh is a firm believer of the technology being the heart of the business. One of his
                        aspiration is to keep the healthy blood pumping in the business for smooth and balanced
                        technological operations.

                    </p>
                </div>
            </div><!-- /.col-* -->

            <div class="col-md-4">
                <div class="card">
                    <img class="rounded-circle w-100" src="{{asset('img/hw.jpg')}}" alt="Fateh Abu Hijleh">
                </div>
                <div class="team-member text-center">
                    <div class="team-member-header">
                        <h4 class="name ubuntu-bold">Ha-Won Jhung</h4>
                        <h4 class="position ubuntu-light">CEO</h4>
                    </div>
                </div>
                <div class="biography text-center pl-3 pr-3">
                    <p class="ubuntu-regular">
                        Being the architect & orchestrator, Ha-Won brings harmony synergy among the & Teams, aiming
                        to
                        create a positive impact in people\'s daily life through digital technology in sports
                        industry.
                    </p>
                </div>
            </div><!-- /.col-* -->

            <div class="col-md-4">
                <div class="card">
                    <img class="rounded-circle w-100" src="{{asset('img/tamer.jpg')}}" alt="Fateh Abu Hijleh">
                </div>
                <div class="team-member text-center">
                    <div class="team-member-header">
                        <h4 class="name ubuntu-bold">Tamer Abu Hijleh</h4>
                        <h4 class="position ubuntu-light">CFO</h4>
                    </div>
                </div>
                <div class="biography text-center pl-3 pr-3">
                    <p class="ubuntu-regular">
                        With the vision of uniting people together through sports and well-being culture, Tamer
                        considers accounting and finance as the solid foundation to scale the business into a
                        sustainable one.

                    </p>
                </div>
            </div><!-- /.col-* -->

        </div><!-- /.row -->
    </div><!-- /#team .container-fluid -->

</div><!-- /#about -->

<div id="contact">

    <div class="container-fluid p-0">

        <div class="top">
            <div class="row">
                <div class="col-md-12 pl-lg-5 pr-lg-5">
                    <h3 class="ubuntu-light">Get In Touch</h3>
                    <p class="ubuntu-light p-0 mt-5 mb-5">You dare because we care.<br>Please do not hesitate to contact
                        us at your convenience should you require any assistance or desire to improve our service.</p>
                </div>

                <div class="col-md-6 mb-3 contact-info-div-left">
                    <p class="ubuntu-bold">Email</p>
                    <div class="row">
                        <div class="col-2">
                            <img class="rounded-circle w-100" src="{{asset('img/contact.png')}}" alt=""/>
                        </div>
                        <div class="col-10 pl-0">
                            <p class="contact-info contact-custom-padding ubuntu-regular">hello@udaresports.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3 contact-info-div-right">
                    <p class="ubuntu-bold">Phone</p>
                    <div class="row">
                        <div class="col-2">
                            <img class="rounded-circle w-100" src="{{asset('img/contact.png')}}" alt=""/>
                        </div>
                        <div class="col-10  pl-0 ">
                            <p class="contact-info contact-custom-padding ubuntu-regular">+971 (0)2 305 2417</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 contact-info-div-left">
                    <p class="ubuntu-bold mt-3" style="padding:0 0 ">Locations</p>
                    <div class="row">
                        <div class="col-2">
                            <img class="rounded-circle w-100" src="{{asset('img/contact.png')}}" alt=""/>
                        </div>
                        <div class="col-10 pl-0">
                            <p class="contact-custom-padding ubuntu-light">Asia</p>
                        </div>
                    </div>
                    <p class="text-white pt-3 ubuntu-regular">Bundang-gu, Seohyeon-ro 210,<br> Seoul, South Korea</p>
                    <img src="{{asset('img/map.png')}}" class="map" alt="map"/>
                </div>
                <div class="col-md-6 contact-info-div-right mt-3">
                    <br>
                    <div class="row">
                        <div class="col-2">
                            <img class="rounded-circle w-100" src="{{asset('img/contact.png')}}" alt=""/>
                        </div>
                        <div class="col-10 pl-0">
                            <p class="contact-custom-padding ubuntu-light">Middle East</p>
                        </div>
                    </div>
                    <p class="pt-3 ubuntu-regular">Krypto labs, Masdar city,<br>Abu Dhabi, United Arab Emirates (UAE)
                    </p>
                    <img src="{{asset('img/map.png')}}" class="map" alt="map"/>
                </div>

            </div><!-- /.row -->
        </div><!-- /.top -->

        <div class="bottom">
            <div class="row">
                <div class="col-lg-2">
                    <h6 class="ubuntu-bold p-lg-2">Privacy Policy</h6>
                </div>
                <div class="col-lg-3">
                    <h6 class="ubuntu-bold p-lg-2">Terms &amp Conditions</h6>
                </div>
                <div class="col-lg-7">
                    <img class="rounded-circle mr-2 float-lg-right" src="{{asset('img/contact.png')}}" alt=""/>
                    <img class="rounded-circle mr-2 float-lg-right" src="{{asset('img/contact.png')}}" alt=""/>
                    <img class="rounded-circle mr-2 float-lg-right" src="{{asset('img/contact.png')}}" alt=""/>
                </div>
            </div><!-- /.row -->
        </div><!-- /.bottom -->

    </div><!-- /.container-fluid -->

</div><!-- /#contact -->


</body>

<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('front/js/bootstrap.min.js')}}"></script>

<script src="{{asset('js/jquery.pagenav.js')}}"></script>
<script>
    $('body').scrollspy({target: '#navbar'})
    jQuery(document).ready(function ($) {
        // Scroll to the desired section on click
        // Make sure to add the `data-scroll` attribute to your `<a>` tag.
        // Example:
        // `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.
        function scrollToSection(event) {
            event.preventDefault();
            var $section = $($(this).attr('href'));
            $('html, body').animate({
                scrollTop: $section.offset().top
            }, 500);
        }

        $('[data-scroll]').on('click', scrollToSection);
    }(jQuery));
    window.onload = responsiveImages;
    window.addEventListener("resize", responsiveImages);

    function responsiveImages() {
        var matches = document.querySelectorAll(".rounded-circle");
        matches.forEach(function (match) {
            match.height = match.width;
        });
        var home = $("#home")
        var carousel =  document.querySelector("#carousel");
        home.height(carousel.clientHeight + carousel.offsetTop)
    }

</script>
@toastr_js
@toastr_render
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--    <link rel="icon" href="images/favicon.ico" type="image/ico"/>--}}
    <title>UDare</title>
    <!-- Bootstrap -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/venue-owner/custom.css') }}" rel="stylesheet">
    <style>
         #first-image {
             background-image: url('{{asset('img/login-1.png')}}');
             background-repeat: no-repeat;
             background-size: cover;
             background-position: right;
         }
        #second-image {
            background-image: url('{{asset('img/login-2.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
        .carousel-item{
            /*height: 900px;*/
        }
    </style>
    @toastr_css
</head>

<body>

<div id="login-carousel" class="carousel slide position-absolute d-none d-lg-block w-100" data-ride="carousel">
    <ol class="carousel-indicators">
        <li class="carousel-indicator active" data-target="#login-carousel" data-slide-to="0"><hr class="vertical-line white"></li>
        <li class="carousel-indicator" data-target="#login-carousel" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" id="first-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div id="welcome-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <p class="mt-5 mb-3 text-center text-white ubuntu-regular" style="font-size: 1.3rem">We provide solutions to all the essentials that you need in order to manage and
                            grow yor Sports and Fitness
                            Venue business
                        </p>
                        <p class="text-center text-white ubuntu-light" style="font-size: 1.2rem; line-height:2.6rem" >
                            Branded Admin. Website Panel &amp App<br>
                            Online Bookings &amp Payment<br>
                            Single &amp Multi Venue Management<br>
                            Memberships &amp Day Passes<br>
                            Promotional Videos<br>
                            Sports Feed<br>
                            Customer Review &amp Ratings<br>
                            Staff &amp Roles Management<br>
                            Financial &amp Reports
                        </p>
                    </div>
                </div>
            </div>
{{--            <img class="d-block w-100" src="{{asset('img/login-1.png')}}" style="position: absolute">--}}
        </div><!-- /.carousel-item -->

        <div class="carousel-item" id="second-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div class="carousel-benefits-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <div class="row">
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.carousel-benefits-section -->
                </div>
            </div>
        </div><!-- /.carousel-item -->

    </div><!-- /.carousel-inner -->
</div><!-- /#login-carousel .carousel -->

<div class="container-fluid" id="login">
    <div class="row p-lg-5 p-md-5 p-0 pt-5">

        <div class="col-lg-5 p-lg-4  p-md-0 p-0">
            <div class="card">
                <a href="{{route('home')}}">
                    <img width="135" height="165" src="{{asset('img/uDareLogo_ad.png')}}" class="img-rounded" alt="uDare">
                </a>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               placeholder="Password" name="password" required autocomplete="current-password">
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="row">
                            <div class="col-12">
                                <a class="reset_pass ubuntu-light" href="{{route('password.request')}}">Forgot your password?</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mt-3">
                                <label for="remember-input" id="remember-label" class="ubuntu-light" style="color: #61676F;font-size: 0.8rem">
                                    <input type="checkbox" name="remember" id="remember-input" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="checkmark" id="remember-checkmark"></span>
                                    {{ __('Remember Me') }}
                                </label>
                                <br>
                            </div>
                            <div class="col-6">
                                <button type="submit" id="login-btn" class="btn ubuntu-light">{{ __('Log In') }}</button>
                            </div>
                        </div>
                    </form>
                    <div class="register-text">
                        <p><a href="{{route('request-venue')}}">Create a new account</a> in quick and easy steps.</p>
                    </div>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div><!-- /.col-* -->

        <div class="col-lg-7 pt-5 d-block d-lg-none">

            <div class="benefits-section">
                <h3 class="text-center">Venues</h3>
                <h2 class="text-center ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                <div class="row">

                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->

                </div><!-- /.row -->
            </div><!-- /.benefits-section -->

        </div><!-- /.col-* -->

    </div><!-- /.row -->
</div><!-- /#login .container-fluid -->


<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('front/js/bootstrap.min.js')}}"></script>
<script>
    window.onload = responsiveImages;
    window.addEventListener("resize", responsiveImages);
    function responsiveImages(){
        var matches = document.querySelectorAll(".set-height");
        matches.forEach(function(match) {
            match.height = match.width;
        });
    }
    window.onload = r;
    window.addEventListener("resize", r);
    function r(){
        $('.carousel-item').addClass('d-lg-block')
        $('.carousel-item').height('auto');
        var matches = document.querySelectorAll(".carousel-item");
        var maxHeight = 0;
        matches.forEach(function(match) {
            if(maxHeight < match.clientHeight){
                maxHeight = match.clientHeight;
            }
        });
        $('.carousel-item').removeClass('d-lg-block')
        var innerHeight = window.innerHeight;
        if(innerHeight > maxHeight){
            maxHeight = innerHeight;
        }
        $('.carousel-item').height(maxHeight);
    }
</script>
</body>
@toastr_js
@toastr_render
</html>

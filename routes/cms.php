<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 22/07/2019
 * Time: 01:28
 */


use Illuminate\Support\Facades\Route;


Route::prefix('cms')->middleware(['permissions'])->group(function () {

    Route::get('/', 'CmsController@index')->name('dashboard');

    Route::resource('country', 'CountryController');
    Route::resource('status', 'StatusController');
    Route::resource('currency', 'CurrencyController');
    Route::resource('user', 'UserController');
//    Route::resource('trainer', 'TrainerController');
    Route::resource('role', 'RoleController');
    Route::resource('sport', 'SportController');
    Route::resource('zone', 'ZoneController');
    Route::resource('venue', 'VenueController');
    Route::resource('link', 'LinkController');
    Route::resource('contract', 'ContractController');
    Route::resource('venue-contract', 'VenueContractController');
    Route::get('venue-contract/{id}/preview', 'VenueContractController@preview')->name('venue-contract.preview');
    Route::get('venue-contract/{id}/download', 'VenueContractController@download')->name('venue-contract.download');
    Route::resource('category', 'CategoryController');
    Route::resource('location', 'LocationController');
    Route::resource('selection', 'SelectionController');
    Route::resource('membership_type', 'MembershipTypeController');
    Route::resource('/{venue}/field', 'FieldController');

    Route::namespace('Front')->group(function () {
        Route::resource('home', 'HomeController');
        Route::resource('solution', 'SolutionController');
        Route::resource('feature', 'FeatureController');
        Route::resource('about', 'AboutController');
        Route::resource('value', 'ValuesController');
        Route::resource('team', 'TeamController');
        Route::resource('registration', 'RegistrationController');
        Route::resource('benefit', 'BenefitsController');
    });



    Route::post('uploader/media', 'CmsController@storeMedia')
        ->name('cms.storeMedia');

});

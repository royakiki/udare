<?php
/**
 * Created by PhpStorm.
 * User: Rabee
 * Date: 22/07/2019
 * Time: 01:28
 */


use Illuminate\Support\Facades\Route;


Route::prefix('admin')->as('admin.')
    ->middleware(['permissions','venue','status'])->group(function () {

    Route::get('/', 'AdminController@index')->name('dashboard');

//    Route::resource('country', 'CountryController');
//    Route::resource('status', 'StatusController');
//    Route::resource('currency', 'CurrencyController');
//    Route::resource('user', 'UserController');
    Route::resource('trainer', 'TrainerController');
//    Route::resource('role', 'RoleController');
//    Route::resource('sport', 'SportController');
//    Route::resource('zone', 'ZoneController');
    Route::resource('venue', 'VenueController');
//    Route::resource('link', 'LinkController');
//    Route::resource('contract', 'ContractController');
//    Route::resource('venue-contract', 'VenueContractController');
//    Route::get('venue-contract/{id}/preview', 'VenueContractController@preview')->name('venue-contract.preview');
//    Route::get('venue-contract/{id}/download', 'VenueContractController@download')->name('venue-contract.download');
//    Route::resource('category', 'CategoryController');
//    Route::resource('location', 'LocationController');
//    Route::resource('selection', 'SelectionController');
    Route::resource('venue/{venue}/field', 'FieldController');
    Route::resource('venue/{venue}/membership', 'MembershipController');
    Route::post('venue/{venue}/field/{id}/image-upload', 'FieldController@uploadImage')->name('field.image-upload');
    Route::post('venue/{venue}/field/{id}/image-remove', 'FieldController@removeImage')->name('field.image-remove');
    Route::get('venue/{venue}/field/{id}/images', 'FieldController@getGallery')->name('field.images');
    Route::post('venue/{id}/image-upload', 'VenueController@uploadImage')->name('venue.image-upload');
    Route::post('venue/{id}/image-remove', 'VenueController@removeImage')->name('venue.image-remove');
    Route::get('venue/{id}/images', 'VenueController@getGallery')->name('venue.images');
    Route::get('venue/{id}/account-details', 'VenueController@editAccountDetails')->name('venue.account-details');
    Route::post('venue/{id}/account-details', 'VenueController@updateAccountDetails')->name('venue.account-details.post');
    Route::get('venue/{id}/content-details', 'VenueController@editContentDetails')->name('venue.content-details');
    Route::post('venue/{id}/content-details', 'VenueController@updateContentDetails')->name('venue.content-details.post');
    Route::resource('field/{field}/slot', 'TimeSlotController');




//    Route::post('uploader/media', 'CmsController@storeMedia')
//        ->name('cms.storeMedia');

});
